using System.Collections.Generic;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.IServices;

namespace ApplicationCore.Services
{
    public class KhachHangService : IKhachHangService
    {
        private IUnitOfWork _unitOfWork;
        public KhachHangService(IUnitOfWork unit)
        {
            _unitOfWork = unit;
        }
        public void Add(KhachHang entity)
        {
            _unitOfWork.KhachHangs.Add(entity);
            _unitOfWork.Complete();
        }

        public void Remove(KhachHang entity)
        {
            _unitOfWork.KhachHangs.Remove(entity);
            _unitOfWork.Complete();
        }

        public void Update(KhachHang entity)
        {
            _unitOfWork.KhachHangs.Update(entity);
            _unitOfWork.Complete();
        }

        public KhachHang GetById(int id)
        {
            return _unitOfWork.KhachHangs.GetById(id);
        }

        public IEnumerable<KhachHang> GetAll()
        {
            return _unitOfWork.KhachHangs.GetAll();
        }

        // public List<KhachHang> GetByName(string HoTen)
        // {
        //     throw new System.NotImplementedException();
        // }

    }
}