using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.IServices;

namespace ApplicationCore.Services
{
    public class ChiTietDonHangService : IChiTietDonHangService
    {
        private IUnitOfWork _unitOfWork;
        public ChiTietDonHangService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Add(ChiTietDonHang entity)
        {
            _unitOfWork.ChiTietDonHangs.Add(entity);
            _unitOfWork.Complete();
        }

        public void AddRane(IEnumerable<ChiTietDonHang> entities)
        {
            _unitOfWork.ChiTietDonHangs.AddRange(entities);
        }

        public IEnumerable<ChiTietDonHang> Find(Expression<Func<ChiTietDonHang, bool>> predicate)
        {
            return _unitOfWork.ChiTietDonHangs.Find(predicate);
        }

        public IEnumerable<ChiTietDonHang> GetAll()
        {
            return _unitOfWork.ChiTietDonHangs.GetAll();
        }

        public ChiTietDonHang GetById(int id)
        {
            return _unitOfWork.ChiTietDonHangs.GetById(id);
        }

        public void Remove(ChiTietDonHang entity)
        {
            _unitOfWork.ChiTietDonHangs.Remove(entity);
        }

        public void RemoveRange(IEnumerable<ChiTietDonHang> entities)
        {
            _unitOfWork.ChiTietDonHangs.RemoveRange(entities);
        }

        public void Update(ChiTietDonHang entity)
        {
            _unitOfWork.ChiTietDonHangs.Update(entity);
            _unitOfWork.Complete();
        }
    }
}