using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Cryptography;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.IServices;

namespace ApplicationCore.Services
{
    public class NhanVienService : INhanVienService
    {
        private readonly IUnitOfWork _unitOfWork;
        public NhanVienService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public bool CheckUserName(string username)
        {
            return _unitOfWork.NhanViens.CheckUserName(username);
        }

        public void Add(NhanVien nhanVien)
        {
            _unitOfWork.NhanViens.Add(nhanVien);
            _unitOfWork.Complete();
        }

        public List<string> CreateSession(string username, string password)
        {
            return _unitOfWork.NhanViens.CreateSession(username, password);
        }

        public void Delete(NhanVien nhanVien)
        {
            _unitOfWork.NhanViens.Remove(nhanVien);
            _unitOfWork.Complete();
        }

        public IEnumerable<NhanVien> Getall()
        {
            return _unitOfWork.NhanViens.GetAll();
        }

        public NhanVien GetById(int id)
        {
            return _unitOfWork.NhanViens.GetById(id);
        }

        public string GetMd5hash(MD5 md5hash, string input)
        {
            return _unitOfWork.NhanViens.GetMd5hash(md5hash, input);
        }


        public void Update(NhanVien nhanVien)
        {
            _unitOfWork.NhanViens.Update(nhanVien);
            _unitOfWork.Complete();
        }

        public bool VerifyMd5Hash(MD5 md5hash, string input, string hash)
        {
            return _unitOfWork.NhanViens.VerifyMd5Hash(md5hash, input, hash);
        }

        public int Login(string TenDangNhap, string MatKhau)
        {
            return _unitOfWork.NhanViens.Login(TenDangNhap, MatKhau);
        }

        public bool CheckPhoneNumberDuplicated(string phoneNumber)
        {
            Expression<Func<KhachHang, bool>> predicate = m => true;
            predicate = m => m.SDT == phoneNumber;
            var KhachHangs = _unitOfWork.KhachHangs.Find(predicate);
            if(KhachHangs == null)
                return true;
            return false;
        }
    }
}