using System.Collections.Generic;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.IServices;

namespace ApplicationCore.Services
{
    public class ThongTinNhapHangService : IThongTinNhapHangService
    {
        private IUnitOfWork _unitOfWork;
        public ThongTinNhapHangService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Add(ThongTinNhapHang entity)
        {
            _unitOfWork.ThongTinNhapHangs.Add(entity);
            _unitOfWork.Complete();
        }

        public IEnumerable<ThongTinNhapHang> GetAll()
        {
            return _unitOfWork.ThongTinNhapHangs.GetAll();
        }

        public ThongTinNhapHang GetById(int id)
        {
            return _unitOfWork.ThongTinNhapHangs.GetById(id);
        }

        public void Remove(ThongTinNhapHang entity)
        {
            _unitOfWork.ThongTinNhapHangs.Remove(entity);
            _unitOfWork.Complete();

        }

      
        public void Update(ThongTinNhapHang entity)
        {
            _unitOfWork.ThongTinNhapHangs.Update(entity);
            _unitOfWork.Complete();
        }
    }
}