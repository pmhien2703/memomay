using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.IServices;
using ApplicationCore.ViewModels;

namespace ApplicationCore.Services
{
    public class SanPhamService : ISanPhamService
    {
        private readonly IUnitOfWork _unitOfWork;
        public SanPhamService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public void Add(SanPham entity)
        {
            _unitOfWork.SanPhams.Add(entity);
            _unitOfWork.Complete();
        }

        public void AddRange(IEnumerable<SanPham> Entities)
        {
            _unitOfWork.SanPhams.AddRange(Entities);
            _unitOfWork.Complete();
        }
        public IEnumerable<SanPham> GetAll()
        {
            return _unitOfWork.SanPhams.GetAll();
        }

        public IEnumerable<SanPhamVM> GetAllByName()
        {
            return _unitOfWork.SanPhams.GetAllByName();
        }

        public List<SanPham> GetAllName()
        {
            return _unitOfWork.SanPhams.GetAllName();
        }

        public SanPham GetById(int id)
        {
            return _unitOfWork.SanPhams.GetById(id);
        }

        public List<SanPham> GetByName(string TenSanPham)
        {
            return _unitOfWork.SanPhams.GetByName(TenSanPham);
        }

        public int GetIdByNameAndSize(string tensp, string size)
        {
            return _unitOfWork.SanPhams.GetIdByNameAndSize(tensp, size);
        }

        public void Remove(SanPham Entity)
        {
            _unitOfWork.SanPhams.Remove(Entity);
            _unitOfWork.Complete();
        }

        public void RemoveByName(string TenSanPham)
        {
            List<SanPham> sanPhams = _unitOfWork.SanPhams.GetByName(TenSanPham);
            foreach (var item in sanPhams)
            {
                item.TrangThai = 0;
                _unitOfWork.SanPhams.Update(item);
            }
            _unitOfWork.Complete();
        }

        public void Update(SanPham Entity)
        {
            _unitOfWork.SanPhams.Update(Entity);
            _unitOfWork.Complete();
        }
        
        public bool CheckNameDuplicated(string TenSanPham)
        {
            var temp = _unitOfWork.SanPhams.GetAllName();
            foreach(var item in temp)
            {
                if(TenSanPham.ToLower() == item.TenSanPham.ToLower() && item.TrangThai ==1)
                    return false;
            }
            return true;
        }
        public bool CheckColor(string TenSanPham,string Color)
        {
            var list = _unitOfWork.SanPhams.GetByName(TenSanPham);
            foreach(SanPham sp in list)
            {
                if(sp.Mau == Color)
                    return false;
            }
            return true;
        }
        //Kiểm tra cả màu và tên
        public bool CheckValidProduct(string TenSanPham,string Color)
        {
            if(CheckNameDuplicated(TenSanPham) || CheckColor(TenSanPham,Color))
                return true;
            return false;
        }
        public int SLN(IEnumerable<SanPham> list)
        {
            int max = 1;
            foreach(var sp in list)
            {
                if(sp.MaSanPham > max)
                    max = sp.MaSanPham;
            }
            return max + 1;
        }
        public int MaSanPham()
        {
            return SLN(_unitOfWork.SanPhams.GetAll());
        }

        public List<SanPham> GetByNameAndColor(string TenSanPham, string Color)
        {
            return _unitOfWork.SanPhams.GetByName(TenSanPham).Where(s => s.Mau == Color).ToList();
        }

        public SanPham GetByNameAndSizeAndColor(string tensp, string size,string color)
        {
            return _unitOfWork.SanPhams.GetByNameAndSizeAndColor(tensp,size,color);
        }
    }
}