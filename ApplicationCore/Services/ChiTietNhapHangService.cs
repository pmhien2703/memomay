using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.IServices;

namespace ApplicationCore.Services
{
    public class ChiTietNhapHangService : IChiTietNhapHangService
    {
        private IUnitOfWork _unitOfWork;
        public ChiTietNhapHangService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Add(ChiTietNhapHang entity)
        {
            _unitOfWork.ChiTietNhapHangs.Add(entity);
            _unitOfWork.Complete();
        }

        public void AddRange(IEnumerable<ChiTietNhapHang> entities)
        {
            _unitOfWork.ChiTietNhapHangs.AddRange(entities);
            _unitOfWork.Complete();

        }

        public IEnumerable<ChiTietNhapHang> Find(Expression<Func<ChiTietNhapHang, bool>> predicate)
        {
            return _unitOfWork.ChiTietNhapHangs.Find(predicate);
        }

        public IEnumerable<ChiTietNhapHang> GetAll()
        {
            return _unitOfWork.ChiTietNhapHangs.GetAll();
        }

        public ChiTietNhapHang GetById(int id)
        {
            return _unitOfWork.ChiTietNhapHangs.GetById(id);
        }

        public void Remove(ChiTietNhapHang entity)
        {
            _unitOfWork.ChiTietNhapHangs.Remove(entity);
            _unitOfWork.Complete();
        }

        public void RemoveRange(IEnumerable<ChiTietNhapHang> entities)
        {
            _unitOfWork.ChiTietNhapHangs.RemoveRange(entities);
            _unitOfWork.Complete();

        }

        public void Update(ChiTietNhapHang entity)
        {
            _unitOfWork.ChiTietNhapHangs.Update(entity);
            _unitOfWork.Complete();
        }
    }
}