using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.IServices;
using ApplicationCore.ViewModels;
namespace ApplicationCore.Services
{
    public class DonHangService : IDonHangService
    {
        private IUnitOfWork _unitOfWork;
        public DonHangService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Add(DonHang donHang)
        {
            _unitOfWork.DonHangs.Add(donHang);
            _unitOfWork.Complete();
        }

        public bool CheckInventoryNumber(int slt, int masp)
        {
            return _unitOfWork.DonHangs.CheckInventoryNumber(slt, masp);
        }

        public bool CheckSizeExist(SanPham sanpham)
        {
            return _unitOfWork.DonHangs.CheckSizeExist(sanpham);
        }

        

        public void Delete(DonHang donHang)
        {
            _unitOfWork.DonHangs.Remove(donHang);
            _unitOfWork.Complete();
        }

        public IEnumerable<DonHang> Find(Expression<Func<DonHang, bool>> predicate)
        {
            return _unitOfWork.DonHangs.Find(predicate);
        }

        // public DonHangCreateVM donHangCreateVM()
        // {
        //     return _unitOfWork.DonHangs.donHangCreateVM();
        // }

        public IEnumerable<DonHang> GetAll()
        {
            return _unitOfWork.DonHangs.GetAll().OrderByDescending(m => m.Ngay.Date).ThenByDescending(x => x.Ngay.TimeOfDay);
        }

        public DonHang GetById(int id)
        {
            return _unitOfWork.DonHangs.GetById(id);
        }

        public void Update(DonHang donHang)
        {
            _unitOfWork.DonHangs.Update(donHang);
            _unitOfWork.Complete();
        }
    }
}