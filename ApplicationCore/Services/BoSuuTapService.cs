using System.Collections.Generic;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.IServices;

namespace ApplicationCore.Services
{
    public class BoSuuTapService : IBoSuuTapService
    {
        private IUnitOfWork _unitOfWork;
        public BoSuuTapService (IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public void Add(BoSuuTap entity)
        {
            _unitOfWork.BoSuuTaps.Add(entity);
            _unitOfWork.Complete();
        }

        public void Remove(BoSuuTap entity)
        {
            _unitOfWork.BoSuuTaps.Remove(entity);
            _unitOfWork.Complete();
        }

        public void Update(BoSuuTap entity)
        {
            _unitOfWork.BoSuuTaps.Update(entity);
            _unitOfWork.Complete();
        }

        public BoSuuTap GetById(int id)
        {
            return _unitOfWork.BoSuuTaps.GetById(id);
        }

        public IEnumerable<BoSuuTap> GetAll()
        {
            return _unitOfWork.BoSuuTaps.GetAll();
        }

    }
}