using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApplicationCore.Interfaces;
using System.ComponentModel;
using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;
namespace ApplicationCore.Entities
{
    public class SanPham : IAggregateRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("Mã sản phẩm")]
        public int MaSanPham {get;set;}
        [DisplayName("Mã bộ sưu tập")]
        public int MaBoSuuTap {get;set;}
        [DisplayName("Tên sản phẩm")]
        public string TenSanPham {get;set;}
        // [DisplayName("Loại sản phẩm")]
        // public string LoaiSanPham {get;set;}
        [DisplayName("Giá gốc")]
        public double GiaGoc {get;set;}
        [DisplayName("Giá bán")]
        public double GiaBan {get;set;}
        public string Size {get;set;}
        [DisplayName("Chất liệu")]
        public string ChatLieu {get;set;}
        [DisplayName("Số lượng tồn")]
        public int SoLuongTon {get;set;}
        [DisplayName("Hình ảnh")]
        public string HinhAnh {get;set;}
        [DisplayName("Màu")]
        public string Mau { get; set; }
        public int TrangThai{ get; set; }
    }
}