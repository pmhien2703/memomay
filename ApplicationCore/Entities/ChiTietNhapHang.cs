using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApplicationCore.Interfaces;

namespace ApplicationCore.Entities
{
    public class ChiTietNhapHang : IAggregateRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("Số thứ tự")]
        public int SoThuTu {get;set;}
        [DisplayName("Mã nhập hàng")]
        public int MaNhapHang {get;set;}
        [DisplayName("Mã sản phẩm")]
        public int MaSanPham {get;set;}
        [DisplayName("Số lượng")]
        public int SoLuong {get;set;}
    }
}