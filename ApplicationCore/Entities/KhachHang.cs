using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApplicationCore.Interfaces;

namespace ApplicationCore.Entities
{
    public class KhachHang : IAggregateRoot
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]        
        public int MaKhachHang {get;set;}
        [DisplayName("Tên khách hàng")]
        public string TenKhachHang { get; set; }
        [DisplayName("Số điện thoại")]
        public string SDT {get;set;}
        [DisplayName("Địa chỉ")]
        public string DiaChi {get;set;}
        [DisplayName("Liên hệ")]
        public string LienHe {get;set;}
    }
}