using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApplicationCore.Interfaces;

namespace ApplicationCore.Entities
{
    public class ThongTinNhapHang : IAggregateRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("Mã nhập hàng")]
        public int MaNhapHang {get;set;}
        [DisplayName("Mã nhân viên")]
        public int MaNhanVien {get;set;}
        [DisplayName("Ngày nhập hàng")]
        public DateTime NgayNhapHang {get;set;}
    }
}
