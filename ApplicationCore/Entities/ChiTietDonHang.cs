using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApplicationCore.Interfaces;

namespace ApplicationCore.Entities
{
    public class ChiTietDonHang : IAggregateRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("Số thứ tự")]
        public int SoThuTu  {get;set;}
        [DisplayName("Mã sản phẩm")]
        public int MaSanPham {get;set;}
        [DisplayName("Mã đơn hàng")]
        public int MaDonHang {get;set;}
        [DisplayName("Thành tiền")]
        public double ThanhTien {get; set;}
        public int SoLuong {get;set;}
        [DisplayName("Chiết khấu")]
        public double ChietKhau { get; set; }
    }
}