using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApplicationCore.Interfaces;

namespace ApplicationCore.Entities
{
    public class NhanVien : IAggregateRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("Mã nhân viên")]
        public int MaNhanVien {get;set;}
        [DisplayName("Họ Tên")]
        public string HoTen {get;set;}
        [DisplayName("Số điện thoại")]
        public string SDT {get;set;}
        [DisplayName("Giới tính")]
        public string GioiTinh {get;set;}
        [DisplayName("Tên đăng nhập")]
        public string TenDangNhap {get;set;}
        [DisplayName("Mật khẩu")]
        public string MatKhau {get;set;}
        [DisplayName("Vai Trò")]
        public string VaiTro{ get; set; }
        [DisplayName("Trạng thái hoạt động")]
        public int TrangThai {get;set;}
    }
}