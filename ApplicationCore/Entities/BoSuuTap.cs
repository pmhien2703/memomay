using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApplicationCore.Interfaces;

namespace ApplicationCore.Entities
{
    public class BoSuuTap : IAggregateRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MaBoSuuTap {get; set;}
        [DisplayName("Tên bộ sưu tập")]
        public string TenBoSuuTap {get;set;}

    }
}