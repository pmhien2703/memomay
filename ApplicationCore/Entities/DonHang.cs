using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApplicationCore.Interfaces;

namespace ApplicationCore.Entities
{
    public class DonHang : IAggregateRoot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("Mã đơn hàng")]
        public int MaDonHang {get;set;}
        [DisplayName("Mã khách hàng")]
        public int MaKhachHang {get;set;}
        [DisplayName("Ngày")]
        public DateTime Ngay {get;set;}
        [DisplayName("Tình trạng")]
        public string TinhTrang {get;set;}
        [DisplayName("Chi phí vận chuyển")]
        public double ChiPhiVanChuyen { get; set; }
        [DisplayName("Phương thức vận chuyển")]
        public string PhuongThucVanChuyen {get;set;}
        [DisplayName("Hình thức thanh toán")]
        public string HinhThucThanhToan { get; set; }
        [DisplayName("Địa chỉ")]
        public string DiaChi {get;set;}
        [DisplayName("Tổng tiền")]
        public double TongTien {get;set;}
        [DisplayName("Ghi chú")]
        public string GhiChu {get;set;}
    }
}