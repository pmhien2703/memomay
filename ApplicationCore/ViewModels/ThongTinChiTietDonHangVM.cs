using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApplicationCore.ViewModels
{
    public class ThongTinChiTietDonHangVM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("Số thứ tự")]
        public int SoThuTu { get; set; }
        [DisplayName("Mã sản phẩm")]
        public int MaSanPham { get; set; }
        [DisplayName("Tên sản phẩm")]
        public string TenSanPham{ get; set; }
        [DisplayName("Thành tiền")]
        public double ThanhTien { get; set; }
        [DisplayName("Số lượng")]
        public int SoLuong { get; set; }
        public string Size{ get; set; }
        [DisplayName("Đơn giá")]
        public double DonGia{ get; set; }
        [DisplayName("Chiết khấu")]
        public double ChietKhau{ get; set; }
        [DisplayName("Màu")]
        public string Mau{ get; set; }
    }
}