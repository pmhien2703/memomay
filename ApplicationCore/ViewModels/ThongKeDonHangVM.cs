using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class ThongKeDonHangVM
    {
        public List<ThongKeDonHangModel> listDonHang { get; set; }
        public int TongSoDonHang { get; set; }
        public double TongTienShip { get; set; }
        public double DoanhThu { get; set; }
        public double TienNhapHang{ get; set; }
        public double LoiNhuan{ get; set; }
    }
}