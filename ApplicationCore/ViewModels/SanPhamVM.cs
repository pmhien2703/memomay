using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApplicationCore.ViewModels
{
    public class SanPhamVM
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DisplayName("Tên bộ sưu tập")]
        public string TenBoSuuTap { get; set; }
        [DisplayName("Tên sản phẩm")]
        public string TenSanPham { get; set; }
        // [DisplayName("Loại sản phẩm")]
        // public string LoaiSanPham { get; set; }
        [DisplayName("Giá bán")]
        public double GiaBan { get; set; }
        [DisplayName("Chất liệu")]
        public string ChatLieu { get; set; }
        [DisplayName("Hình ảnh")]
        public string HinhAnh { get; set; }
        [DisplayName]
        public string Mau{ get; set; }
    }
}