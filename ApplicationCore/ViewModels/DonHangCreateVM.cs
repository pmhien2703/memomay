using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class DonHangCreateVM
    {
        public List<SanPham> ThongTinSP{ get; set; }
        public IEnumerable<KhachHang> ThongTinKH{ get; set; }
        public List<ThongTinChiTietDonHangVM> ThongTinCTDH{ get; set; }
        public SanPham sanPham{ get; set; }
        public double ChietKhau{ get; set; }
        public KhachHang khachHang{ get; set; }
        public DonHang ThongTinDH{ get; set; }
        public double TongTien{ get; set; }
    }
}