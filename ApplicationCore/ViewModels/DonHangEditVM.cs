using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class DonHangEditVM
    {
        public DonHang donHang { get; set; }
        public string TenKhachHang { get; set; }
        public IEnumerable<ThongTinChiTietDonHangVM> listCTDT { get; set; }
    }
}