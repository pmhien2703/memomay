namespace ApplicationCore.ViewModels
{
    public class ChiTietNhapHangVM
    {
        public string TenSanPham{ get; set; }
        public int MaSanPham{get;set;}
        public string Mau{get;set;}
        public string Ri{ get; set; }
        public int Size{ get; set; }
        public int SoLuong{ get; set; }
    }
}