using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class ThongKeSanPhamVM
    {
        public string TenSanPham{ get; set; }
        public List<int> SoLuongMoiSize{ get; set; }
        public string img{ get; set; }
        public int SoLuongSanPham{ get; set; }
    }
}