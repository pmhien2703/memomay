namespace ApplicationCore.ViewModels
{
    public class KhachHangNameandIDVM
    {
        public int MaKhachHang { get; set; }
        public string TenKhachHang { get; set; }
    }
}