using System.ComponentModel;
using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class KhachHangVM
    {
        public KhachHang khachHang { get; set; }
        [DisplayName("Số đơn hàng")]
        public int soLuongDonHang { get; set; }
        [DisplayName("Tổng tiền")]
        public double tongTien{ get; set; }
    }
}