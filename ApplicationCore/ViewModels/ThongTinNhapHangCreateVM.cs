using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class ThongTinNhapHangCreateVM
    {
        public IEnumerable<SanPham> SanPhams{ get; set; }
        // public ThongTinNhapHang TTNH { get; set; }
        public List<ChiTietNhapHangVM> ListChiTietNhapHangs{ get; set; }
        public ChiTietNhapHangVM chiTietNhapHangVM{ get; set; }
        public SanPham sanPham{ get; set; }
    }
}