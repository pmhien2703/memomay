using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class DonHangIndexVM
    {
        public PaginatedList<DonHang> DonHangs { get; set; }
    }
}