using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class SanPhamEditVM
    {
        public SanPham sanPham { get; set; }
        public IEnumerable<BoSuuTap> listBST { get; set; }
        public double Gia1 { get; set; }
        public double Gia2 { get; set; }
        public double Gia3 { get; set; }

        public int hangTonSize1 {get; set; }
        public int hangTonSize2 {get; set; }
        public int hangTonSize3 {get; set; }
        public int hangTonSize4 {get; set; }
        public int hangTonSize5 {get; set; }
        public int hangTonSize6 {get; set; }
        public int hangTonSize7 {get; set; }
        public int hangTonSize8 {get; set; }
        public int hangTonSize9 {get; set; }
        public int hangTonSize10 {get; set; }
        public int hangTonSize11 {get; set; }
        public int hangTonSize12 { get; set; }
    }
}