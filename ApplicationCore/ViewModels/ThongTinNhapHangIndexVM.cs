using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class ThongTinNhapHangIndexVM
    {
        public PaginatedList<ThongTinNhapHang> ThongTinNhapHangs { get; set; }

    }
}