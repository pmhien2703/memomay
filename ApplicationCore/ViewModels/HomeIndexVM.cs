namespace ApplicationCore.ViewModels
{
    public class HomeIndexVM
    {
        public double DoanhThuThangHienTai { get; set; }
        public double PhanTramDoanhThu { get; set; }
        public int SoLuongKhachHang { get; set; }
        public int SoLuongDonHang { get; set; }
        public double PhanTramDonHang{ get; set; }
    }
}