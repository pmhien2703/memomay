using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class SanPhamIndexVM
    {
        public IEnumerable<BoSuuTap> listBST{ get; set; }
        public PaginatedList<SanPhamVM> SanPhams { get; set; }
    }
}