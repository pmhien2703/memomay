using ApplicationCore.Entities;

namespace ApplicationCore.ViewModels
{
    public class ThongKeDonHangModel
    {
        public DonHang DonHang { get; set; }
        public double GiaGoc { get; set; }
    }
}