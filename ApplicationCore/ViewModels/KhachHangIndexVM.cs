namespace ApplicationCore.ViewModels
{
    public class KhachHangIndexVM
    {
        public PaginatedList<KhachHangVM> KhachHangs { get; set; }
    }
}