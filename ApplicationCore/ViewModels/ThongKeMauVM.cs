namespace ApplicationCore.ViewModels
{
    public class ThongKeMauVM
    {
        public string TenMau { get; set; }
        public int SoLuong{ get; set; }
    }
}