using System.Collections.Generic;
using ApplicationCore.Entities;
using System;
using System.Collections;
using System.ComponentModel;

namespace ApplicationCore.ViewModels
{
    public class SanPhamCreateVM
    {
        public SanPham sanPham { get; set; }
        public IEnumerable<BoSuuTap> listBST { get; set; }
    }
}