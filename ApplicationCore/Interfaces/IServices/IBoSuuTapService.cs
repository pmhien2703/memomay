using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IServices
{
    public interface IBoSuuTapService
    {
        public void Add(BoSuuTap entity);
        public void Remove(BoSuuTap entity);
        public void Update(BoSuuTap entity);
        public BoSuuTap GetById(int id);
        public IEnumerable<BoSuuTap> GetAll();
    }
}