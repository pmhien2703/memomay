using System.Collections.Generic;
using System.Security.Cryptography;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IServices
{
    public interface INhanVienService
    {
        public int Login(string TenDangNhap, string MatKhau);
        public string GetMd5hash(MD5 md5hash, string input);
        public bool VerifyMd5Hash(MD5 md5hash, string input, string hash);
        public List<string> CreateSession(string username, string password);

        public bool CheckUserName(string username);
        public void Add(NhanVien nhanVien);
        public void Delete(NhanVien nhanVien);
        public NhanVien GetById(int id);
        public void Update(NhanVien nhanVien);
        public IEnumerable<NhanVien> Getall();
        public bool CheckPhoneNumberDuplicated(string phoneNumber);
    }
}