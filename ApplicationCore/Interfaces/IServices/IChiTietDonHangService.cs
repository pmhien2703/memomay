using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IServices
{
    public interface IChiTietDonHangService
    {
        public void Add(ChiTietDonHang entity);
        public void AddRane(IEnumerable<ChiTietDonHang> entities);
        public void Remove(ChiTietDonHang entity);
        public void RemoveRange(IEnumerable<ChiTietDonHang> entities);
        public void Update(ChiTietDonHang entity);
        public ChiTietDonHang GetById(int id);
        public IEnumerable<ChiTietDonHang> GetAll();
        public IEnumerable<ChiTietDonHang> Find(Expression<Func<ChiTietDonHang, bool>> predicate);
    }
}