using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IServices
{
    public interface IThongTinNhapHangService
    {
        public void Add(ThongTinNhapHang entity);
        public void Remove(ThongTinNhapHang entity);
        public void Update(ThongTinNhapHang entity);
        public ThongTinNhapHang GetById(int id);
        public IEnumerable<ThongTinNhapHang> GetAll();
    }
}