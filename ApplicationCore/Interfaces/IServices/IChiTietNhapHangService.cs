using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IServices
{
    public interface IChiTietNhapHangService
    {
        public void Add(ChiTietNhapHang entity);
        public void AddRange(IEnumerable<ChiTietNhapHang> entities);
        public void Remove(ChiTietNhapHang entity);
        public void RemoveRange(IEnumerable<ChiTietNhapHang> entities);
        public void Update(ChiTietNhapHang entity);
        public ChiTietNhapHang GetById(int id);
        public IEnumerable<ChiTietNhapHang> GetAll();
        public IEnumerable<ChiTietNhapHang> Find(Expression<Func<ChiTietNhapHang, bool>> predicate);
    }
}