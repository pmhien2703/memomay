using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using ApplicationCore.Entities;
using ApplicationCore.ViewModels;

namespace ApplicationCore.Interfaces.IServices
{
    public interface IDonHangService
    {
        public void Add(DonHang donHang);
        public void Delete(DonHang donHang);
        public void Update(DonHang donHang);
        //public void Find()
        public DonHang GetById(int id);
        public IEnumerable<DonHang> GetAll();
        //public DonHangCreateVM donHangCreateVM();
        public bool CheckSizeExist(SanPham sanPham);
        public bool CheckInventoryNumber(int slt, int masp);
        public IEnumerable<DonHang> Find(Expression<Func<DonHang, bool>> predicate);
        //public DonHangCreateVM CreateDHVM(DonHangCreateVM donHang);
    }
}