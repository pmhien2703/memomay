using System.Collections.Generic;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IServices
{
    public interface IKhachHangService
    {
        public KhachHang GetById(int id);
        public IEnumerable<KhachHang> GetAll();
        public void Remove(KhachHang Entity);
        public void Add(KhachHang entity);
        public void Update(KhachHang Entity);
        //public List<KhachHang> GetByName(string HoTen);
    }
}