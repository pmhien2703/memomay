using System.Collections.Generic;
using ApplicationCore.Entities;
using ApplicationCore.ViewModels;

namespace ApplicationCore.Interfaces.IServices
{
    public interface ISanPhamService
    {
        public SanPham GetById(int id);
        public IEnumerable<SanPham> GetAll();
        public void Remove(SanPham Entity);
        public void Add(SanPham entity);
        public void AddRange(IEnumerable<SanPham> Entities);
        public void Update(SanPham Entity);
        public List<SanPham> GetByName(string TenSanPham);
        public IEnumerable<SanPhamVM> GetAllByName();
        public List<SanPham> GetAllName();
        public int GetIdByNameAndSize(string tensp, string size);
        // public SanPhamCreateVM CreateNewProduct();
        public void RemoveByName(string TenSanPham);
        public bool CheckNameDuplicated(string TenSanPham);
        public bool CheckValidProduct(string TenSanPham, string Color);
        public int MaSanPham();
        public List<SanPham> GetByNameAndColor(string TenSanPham, string Color);
        public SanPham GetByNameAndSizeAndColor(string tensp, string size,string color);
    }
}