using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IRepository
{
    public interface IThongTinNhapHangRepository : IRepository<ThongTinNhapHang>
    {
         
    }
}