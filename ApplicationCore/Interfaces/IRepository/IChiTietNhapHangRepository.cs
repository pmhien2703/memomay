using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IRepository
{
    public interface IChiTietNhapHangRepository : IRepository<ChiTietNhapHang>
    {
         
    }
}