using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IRepository
{
    public interface IKhachHangRepository : IRepository<KhachHang>
    {
        //public List<KhachHang> GetByName(string HoTen);
    }
}