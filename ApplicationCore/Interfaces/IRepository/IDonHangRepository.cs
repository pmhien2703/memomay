using System.Collections.Generic;
using ApplicationCore.Entities;
using ApplicationCore.ViewModels;

namespace ApplicationCore.Interfaces.IRepository
{
    public interface IDonHangRepository : IRepository<DonHang>
    {
        //public DonHangCreateVM donHangCreateVM();
        public bool CheckSizeExist(SanPham sanpham);
        public bool CheckInventoryNumber(int slt, int masp);
        public void CreateDonHang(DonHangCreateVM donHangCreateVM);
    }
}