using System.Collections.Generic;
using System.Security.Cryptography;
using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IRepository
{
    public interface INhanVienRepository : IRepository<NhanVien>
    {
        public int Login(string TenDangNhap, string MatKhau);
        public string GetMd5hash(MD5 md5hash, string input);
        public bool VerifyMd5Hash(MD5 md5hash, string input, string hash);
        public List<string> CreateSession(string username, string password);
        public bool CheckUserName(string username);
    }
}