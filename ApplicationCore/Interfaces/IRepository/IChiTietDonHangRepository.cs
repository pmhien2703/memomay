using ApplicationCore.Entities;

namespace ApplicationCore.Interfaces.IRepository
{
    public interface IChiTietDonHangRepository : IRepository<ChiTietDonHang>
    {
         
    }
}