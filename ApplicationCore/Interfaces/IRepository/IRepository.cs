using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ApplicationCore.Interfaces.IRepository
{
    public interface IRepository<T> where T : IAggregateRoot
    {   
         T GetById(int id);
         IEnumerable<T> GetAll();
         void Remove(T Entity);
         void RemoveRange(IEnumerable<T> Entities);
         void Add(T entity);
         void AddRange(IEnumerable<T> Entities);
         void Update(T Entity);
         IEnumerable<T> Find(Expression<Func<T,bool>> predicate);
    }
}