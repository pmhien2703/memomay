using System.Collections.Generic;
using ApplicationCore.Entities;
using ApplicationCore.ViewModels;

namespace ApplicationCore.Interfaces.IRepository
{
    public interface ISanPhamRepository : IRepository<SanPham>
    {
        public List<SanPham> GetByName(string TenSanPham);
        public IEnumerable<SanPhamVM> GetAllByName();
        public List<SanPham> GetAllName();
        public int GetIdByNameAndSize(string tensp, string size);
        // public SanPhamCreateVM CreateNewProduct();
        //public void RemoveByName(string TenSanPham);
        public SanPham GetByNameAndSizeAndColor(string tensp, string size, string color);

    }
}