using System;
using ApplicationCore.Interfaces.IRepository;

namespace ApplicationCore.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IBoSuuTapRepositoty BoSuuTaps { get; }
        IChiTietDonHangRepository ChiTietDonHangs { get; }
        IChiTietNhapHangRepository ChiTietNhapHangs { get;}
        IDonHangRepository DonHangs { get; }
        IKhachHangRepository KhachHangs { get; }
        INhanVienRepository NhanViens { get; }
        ISanPhamRepository SanPhams { get; }
        IThongTinNhapHangRepository ThongTinNhapHangs { get; }
        int Complete();
    }
}