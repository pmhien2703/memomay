
using ApplicationCore.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class MeMoMay : DbContext
    {
        public MeMoMay (DbContextOptions<MeMoMay> options) : base (options){}
        public DbSet<BoSuuTap> BoSuuTaps {get; set;}
        public DbSet<ChiTietDonHang> ChiTietDonHangs {get; set;}
        public DbSet<ChiTietNhapHang> ChiTietNhapHangs {get; set;}
        public DbSet<DonHang> DonHangs {get; set;}
        public DbSet<KhachHang> KhachHangs {get; set;}
        public DbSet<NhanVien> NhanViens {get; set;}
        public DbSet<SanPham> SanPhams {get; set;}
        public DbSet<ThongTinNhapHang> ThongTinNhapHangs {get; set;}
    }
}