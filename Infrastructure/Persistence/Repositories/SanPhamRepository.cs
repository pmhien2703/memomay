using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IRepository;
using ApplicationCore.ViewModels;
using Microsoft.EntityFrameworkCore;
namespace Infrastructure.Persistence.Repositories
{
    public class SanPhamRepository : Repository<SanPham>, ISanPhamRepository
    {
        public SanPhamRepository(MeMoMay context) : base(context)
        {
        }

        public MeMoMay context
        {
            get { return Context as MeMoMay; }
        }

        // public SanPhamCreateVM CreateNewProduct()
        // {
        //     var result = from bst in context.BoSuuTaps
        //                 select new SanPhamCreateVM
        //                 {
        //                     listBST = bst.
        //                 }
        // }

        public IEnumerable<SanPhamVM> GetAllByName()
        {
            var result = from sp in context.SanPhams
                          join bst in context.BoSuuTaps
                          on sp.MaBoSuuTap equals bst.MaBoSuuTap
                          where sp.Size.Equals("1") && sp.TrangThai == 1
                          select new SanPhamVM
                          {
                              TenBoSuuTap = bst.TenBoSuuTap,
                              TenSanPham = sp.TenSanPham,
                              ChatLieu = sp.ChatLieu,
                              GiaBan = sp.GiaBan,
                              HinhAnh = sp.HinhAnh,
                              Mau = sp.Mau
                          };
                return result.OrderBy(t => t.TenSanPham).ToList();
        }

        public List<SanPham> GetAllName()
        {
            var result = from sp in context.SanPhams
                         where sp.Size.Equals("1") && sp.TrangThai == 1
                         select sp;
            return result.ToList();
        }

        public List<SanPham> GetByName(string TenSanPham)
        {
            Expression<Func<SanPham, bool>> predicate = m => true;
            predicate = m => m.TenSanPham == TenSanPham;
            return Find(predicate).Where(h=>h.TrangThai == 1).ToList();
        }

        public int GetIdByNameAndSize(string tensp, string size)
        {
            var result = from sp in context.SanPhams
                         where sp.Size.Equals(size) && sp.TenSanPham.Equals(tensp)
                         select sp.MaSanPham;
            int id = 0;
            foreach(var item in result)
            {
                id = Int32.Parse(item.ToString());
            }
            return id;
        }
        public SanPham GetByNameAndSizeAndColor(string tensp, string size, string color)
        {
            SanPham result = (from sp in context.SanPhams
                         where sp.Size.Equals(size) && sp.TenSanPham.Equals(tensp) && sp.Mau.Equals(color)
                         select sp).First();
            return result;
        }


        public void RemoveByName(string TenSanPham)
        {
            List<SanPham> sanPhams = GetByName(TenSanPham);
            foreach(var item in sanPhams)
            {
                item.TrangThai = 0;
            }
        }
    }
}