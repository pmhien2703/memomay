using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IRepository;

namespace Infrastructure.Persistence.Repositories
{
    public class ChiTietNhapHangRepository : Repository<ChiTietNhapHang>,IChiTietNhapHangRepository
    {
        public ChiTietNhapHangRepository(MeMoMay context) : base (context)
        {

        }
        public MeMoMay context
        {
            get { return Context as MeMoMay; }
        }
        
    }
}