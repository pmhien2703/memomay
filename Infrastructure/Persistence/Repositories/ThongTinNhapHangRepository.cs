using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IRepository;

namespace Infrastructure.Persistence.Repositories
{
    public class ThongTinNhapHangRepository : Repository<ThongTinNhapHang>, IThongTinNhapHangRepository
    {
        public ThongTinNhapHangRepository(MeMoMay context) : base (context)
        {

        }
        public MeMoMay context
        {
            get { return Context as MeMoMay; }
        }
        
    }
}