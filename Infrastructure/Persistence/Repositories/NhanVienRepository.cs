using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IRepository;

namespace Infrastructure.Persistence.Repositories
{
    public class NhanVienRepository : Repository<NhanVien>, INhanVienRepository
    {
        public NhanVienRepository(MeMoMay context) : base(context)
        {
            
        }
        public MeMoMay MeMoMay
        {
            get { return Context as MeMoMay; }
        }

        public bool CheckUserName(string username)
        {
            Expression<Func<NhanVien, bool>> predicate = m => true;
            predicate = m => m.TenDangNhap == username;
            NhanVien nhanVien = Find(predicate).FirstOrDefault();
            if(nhanVien == null)
                return true;
            else
                return false;   
        }

        public List<string> CreateSession(string TenDangNhap, string MatKhau)
        {
            // mã hóa password trước khi tìm kiếm
            MD5 md5hash = MD5.Create();
            var hasPassword = GetMd5hash(md5hash, MatKhau);

            List<string> listSession = new List<string>();

            Expression<Func<NhanVien, bool>> predicate = m => true;
            predicate = m => m.TenDangNhap == TenDangNhap;

            NhanVien nhanVien = Find(predicate).FirstOrDefault();
            string tennhanvien = nhanVien.HoTen;
            string VaiTro = nhanVien.VaiTro;
            int manhanvien = nhanVien.MaNhanVien;
            int TrangThai = nhanVien.TrangThai;
            listSession.Add(tennhanvien);
            listSession.Add(manhanvien.ToString());
            listSession.Add(VaiTro.ToString());
            listSession.Add(TrangThai.ToString());
            return listSession;
        }

        public string GetMd5hash(MD5 md5hash, string input)
        {
            byte[] data = md5hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("X2"));
            }
            return sb.ToString().ToLower();
        }

        public int Login(string TenDangNhap, string MatKhau)
        {
            Expression<Func<NhanVien, bool>> predicate = m => true;
            predicate = m => m.TenDangNhap == TenDangNhap;
            
            NhanVien nhanVien =  Find(predicate).FirstOrDefault();
            
            string tenDangNhapSql;
            string matKhauSql;
            if(nhanVien == null)
            {
                return 0;
            }
            else if(nhanVien.TrangThai == 0)
            {
                return 1;
            }
            else
            {
                tenDangNhapSql = nhanVien.TenDangNhap;
                matKhauSql = nhanVien.MatKhau;
            }
            using (MD5 md5hash = MD5.Create())
            {
                if (VerifyMd5Hash(md5hash, MatKhau, matKhauSql) && tenDangNhapSql == TenDangNhap)
                {
                    return 2;
                }
                else
                {
                    return 3;
                }
            }

        }

        public bool VerifyMd5Hash(MD5 md5hash, string input, string hash)
        {
            var hashOfInput = GetMd5hash(md5hash, input);
            if (String.Compare(hash, hashOfInput) == 0)
            {
                return true;
            }
            return false;
        }
    }
}