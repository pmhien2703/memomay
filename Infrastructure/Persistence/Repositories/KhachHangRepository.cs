using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IRepository;

namespace Infrastructure.Persistence.Repositories
{
    public class KhachHangRepository : Repository<KhachHang>,IKhachHangRepository
    {
        public KhachHangRepository(MeMoMay context) : base (context)
        {

        }
        public MeMoMay context
        {
            get { return Context as MeMoMay; }
        }
    }
}