using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IRepository;

namespace Infrastructure.Persistence.Repositories
{
    public class ChiTietDonHangRepository : Repository<ChiTietDonHang>, IChiTietDonHangRepository
    {
        public ChiTietDonHangRepository(MeMoMay context) : base (context)
        {

        }
        public MeMoMay context
        {
            get { return Context as MeMoMay; }
        }
    }
}