using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IRepository;

namespace Infrastructure.Persistence.Repositories
{
    public class BoSuuTapRepository : Repository<BoSuuTap>, IBoSuuTapRepositoty
    {
        public BoSuuTapRepository(MeMoMay context) : base(context)
        {

        }   
        public MeMoMay context
        {
            get { return Context as MeMoMay; }
        }     
    }
}