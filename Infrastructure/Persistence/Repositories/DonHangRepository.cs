using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IRepository;
using ApplicationCore.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
namespace Infrastructure.Persistence.Repositories
{
    public class DonHangRepository : Repository<DonHang>, IDonHangRepository
    {
        public DonHangRepository(MeMoMay context) : base(context)
        {

        }
        public MeMoMay context
        {
            get { return Context as MeMoMay; }
        }

        public bool CheckInventoryNumber(int slt, int masp)
        {
            var inventoryNumber = from sp in context.SanPhams
                                  where sp.MaSanPham == masp
                                  select sp.SoLuongTon;
            int sltsql = 0;
            foreach(var item in inventoryNumber)
            {
             sltsql = Int32.Parse(item.ToString());
            }
            if( sltsql  >= slt)
            {
                return true;
            }
            return false;
        }

        public bool CheckSizeExist(SanPham sanpham)
        {
            var listSize = from sp in context.SanPhams
                           where sp.TenSanPham.Equals(sanpham.TenSanPham)
                           select sp.Size;
            foreach(var item in listSize)
            {
                if(sanpham.Size == item) return true;
            }
            return false;
        }

        public void CreateDonHang(DonHangCreateVM donHangCreateVM)
        {
            

        }
    }
}