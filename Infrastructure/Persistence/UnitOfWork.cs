using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.IRepository;
using Infrastructure.Persistence.Repositories;

namespace Infrastructure.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MeMoMay _context;
        public UnitOfWork(MeMoMay context)
        {
            SanPhams = new SanPhamRepository(context);
            KhachHangs = new KhachHangRepository(context);
            NhanViens = new NhanVienRepository(context);
            DonHangs = new DonHangRepository(context);
            BoSuuTaps = new BoSuuTapRepository(context);
            ChiTietNhapHangs = new ChiTietNhapHangRepository(context);
            ChiTietDonHangs = new ChiTietDonHangRepository(context);
            ThongTinNhapHangs = new ThongTinNhapHangRepository(context);
            _context = context;
        }
        public IDonHangRepository DonHangs { get; private set; }
        public IBoSuuTapRepositoty BoSuuTaps { get; private set; }

        public IChiTietDonHangRepository ChiTietDonHangs { get; private set; }
        public IChiTietNhapHangRepository ChiTietNhapHangs { get; private set; }

        public IKhachHangRepository KhachHangs { get; private set; }

        public INhanVienRepository NhanViens { get; private set; }

        public ISanPhamRepository SanPhams { get; private set; }

        public IThongTinNhapHangRepository ThongTinNhapHangs { get; private set; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}