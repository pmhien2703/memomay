var nhanVien = {
    intit: function () {
        nhanVien.registerEventsChangeStatus();
        nhanVien.registerEventsChangeRole();
    },
    registerEventsChangeStatus: function () {
        $(document).on('change', '.btn-changestatus', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var status = $(this).val();
            alert(status);
            alert(id);
            $.ajax({
                url: "/NhanVien/ChangeStatus",
                data: { id: id, status: status},
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response == true) {
                        $(".table").load(location.href + " .table");
                        alert("Success");
                    }
                    else {
                        alert("False");
                    }
                }
            });
        });
    },
    registerEventsChangeRole: function () {
        $(document).on('change', '.btn-change-role', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var role = $(this).val();
            alert(id);
            alert(role);    
            $.ajax({
                url: "/NhanVien/ChangeRole",
                data: { id: id,role : role },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response == true) {
                        $(".table").load(location.href + " .table");
                        alert("Success");
                    }
                    else {
                        alert("False");
                    }
                }
            });
        });
    }
}
nhanVien.intit();