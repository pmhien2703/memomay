var donHang = {
    intit: function () {
        donHang.registerEventsChangeStatus();
        donHang.registerEventsChangeSize();
        donHang.registerEventsChangeQuantity();
        donHang.registerEventsRemove();
        donHang.registerEventsChangeDiscount();
    },
    registerEventsChangeStatus: function () {
        $(document).on('change', '.btn-changestatus', function (e) {
            e.preventDefault();
                var id = $(this).data('id');
                var status = $(this).val();
                var pageindex = $(this).data('pageindex');
            var sort = $(this).data('sort');
            alert(status);
                $.ajax({
                    url: "/DonHang/ChangeStatus",
                    data: { id: id, status: status, pageindex: pageindex ,sort:sort},
                    dataType: "json",
                    type: "POST",
                    success: function (response) {
                        console.log(response);
                        if (response == true) {
                            $(".table").load(location.href + " .table");
                            alert("success");
                        }
                        else {
                            alert("False");
                        }
                    }
                });
        });
    },
    registerEventsChangeQuantity: function () {
        $(document).on('change', '.btn-change-quantity', function (e) {
            e.preventDefault();
            var id = $(this).data('stt');
            var quantity = $(this).val();
            var idsanpham = $(this).data('idsanpham');
            $.ajax({
                url: "/DonHang/ChangeQuantity",
                data: { id: id, quantity: quantity, idsanpham:idsanpham },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response == true) {
                        $("#donHang").load(location.href + " #donHang");
                        alert("Success");
                    }
                    else {
                        alert("False");
                    }
                }
            });
        });
    },
    registerEventsChangeSize: function () {
        $(document).on('change', '.btn-change-size', function (e) {
            e.preventDefault();
            var stt = $(this).data('stt');
            var size = $(this).val();
            var idsanpham = $(this).data('idsanpham');
            $.ajax({
                url: "/DonHang/ChangeSize",
                data: { stt: stt, size: size, idsanpham: idsanpham },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response == true) {
                        $("#donHang").load(location.href + " #donHang");
                        alert("Success");
                    }
                    else {
                        alert("False");
                    }
                }
            });
        });
    },
    registerEventsChangeDiscount: function () {
        $(document).on('change', '.btn-change-discount', function (e) {
            e.preventDefault();
            var id = $(this).data('stt');
            var discount = $(this).val();
            alert(id);
            alert(discount);
            $.ajax({
                url: "/DonHang/ChangeDiscount",
                data: { id: id, discount: discount },
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response == true) {
                        $("#donHang").load(location.href + " #donHang");
                        alert("Success");
                    }
                    else {
                        alert("False");
                    }
                }
            });
        });
    },
    registerEventsRemove: function () {
        $(document).off('click').on('click', '.btn-remove', function (e) {
            e.preventDefault();
            var id = $(this).data('stt');
            $.ajax({
                url: "/DonHang/RemoveOrderLine",
                data: { id: id},
                dataType: "json",
                type: "POST",
                success: function (response) {
                    console.log(response);
                    if (response == true) {
                        $("#donHang").load(location.href + " #donHang");
                        alert("Success");
                    }
                    else {
                        alert("False");
                    }
                }
            });
        });
    },
}
donHang.intit();
$(document).ready(function () {
    $("#KHSelect").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '/DonHang/AutoCompleteCustomer',
                dataType: "json",
                type: "POST",
                data: { Prefix: $("#KHSelect").val() },
                success: function (data) {
                    response($.map(data, function (item) {
                        $("#MaKhachHang").val(item.maKhachHang);
                        $("#DiaChi").val(item.diaChi);
                        return { label: item.tenKhachHang + " - " + item.sdt, value: item.tenKhachHang + " - " + item.sdt};
                       
                    }))  
                },
                error: function (xhr, status, error) {
                    alert("Error");
                }
            });
        }
    });
});
