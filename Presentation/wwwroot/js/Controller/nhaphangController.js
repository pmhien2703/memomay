var nhapHang = {
    intit: function () {
        nhapHang.registerEvents();
        $('#select-ri').prop('disabled', 'disabled');
        $('#select-size').prop('disabled', false);
    },
    registerEvents: function () {
        $(document).off('change').on('change', '#btn-choise', function (e) {
            e.preventDefault();
            var choise = $(this).val();            
            if (choise == "Ri")
            {
                $("#size").css('visibility', 'hidden');
                $("#ri").css('visibility', 'visible');
                $("#select-size").prop('disabled', 'disabled');
                $("#select-ri").prop('disabled', false);
            }
            else
            {
                $("#size").css('visibility', 'visible');
                $("#ri").css('visibility', 'hidden');
                $("#select-ri").prop('disabled', 'disabled');
                $("#select-size").prop('disabled', false);

            }
        });
    }
}
nhapHang.intit();