-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 03, 2019 lúc 12:18 PM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `coffeeshop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill`
--

CREATE TABLE `bill` (
  `ID` int(8) NOT NULL,
  `DateTime` datetime NOT NULL,
  `Total` double NOT NULL,
  `Type` varchar(15) NOT NULL,
  `StaffID` int(4) NOT NULL,
  `MemberID` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `billdetail`
--

CREATE TABLE `billdetail` (
  `BillID` int(8) NOT NULL,
  `No` int(11) NOT NULL,
  `ProductID` int(3) NOT NULL,
  `Price` int(11) NOT NULL,
  `Quantity` int(3) NOT NULL,
  `Discount` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `ID` int(2) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`ID`, `Name`) VALUES
(1, 'coffee'),
(2, 'tea'),
(3, 'smoothie');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `member`
--

CREATE TABLE `member` (
  `ID` int(5) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Phone` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `member`
--

INSERT INTO `member` (`ID`, `Username`, `Password`, `Name`, `Email`, `Address`, `Status`, `Phone`) VALUES
(2, 'pmhien2703', '12345678', 'Hien Pham', 'pmhien2703@gmail.com', '15/4/8 Lê Lai, phường 12, quận Tân Bình', 1, '0922638605'),
(3, 'pmhien271', '12345678', 'Minh Hien', 'dnhq1108@gmail.com', '15/4/8 Lê Lai, phường 12, quận Tân Bình', 1, '0922638605');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `ID` int(4) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Price` double NOT NULL,
  `Image` varchar(255) NOT NULL,
  `Size` char(1) NOT NULL,
  `CategoryID` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`ID`, `Name`, `Price`, `Image`, `Size`, `CategoryID`) VALUES
(1, 'coffee', 2, 'assets/images/cupofcoffee.jfif', 'm', 1),
(2, 'coffee', 2.5, 'assets/images/cupofcoffee.jfif', 'l', 1),
(3, 'white coffee', 2.25, 'assets/images/cupofcoffee.jfif', 'm', 1),
(4, 'white coffee', 2.75, 'assets/images/cupofcoffee.jfif', 'l', 1),
(5, 'milk coffee', 2.25, 'assets/images/cupofcoffee.jfif', 'm', 1),
(6, 'milk coffee', 2.75, 'assets/images/cupofcoffee.jfif', 'l', 1),
(7, 'espresso', 2, 'assets/images/cupofcoffee.jfif', 's', 1),
(8, 'espresso', 2.5, 'assets/images/cupofcoffee.jfif', 'm', 1),
(9, 'americano', 2, 'assets/images/cupofcoffee.jfif', 's', 1),
(10, 'americano', 2.5, 'assets/images/cupofcoffee.jfif', 'm', 1),
(11, 'americano', 3, 'assets/images/cupofcoffee.jfif', 'l', 1),
(12, 'capuchino', 2.5, 'assets/images/cupofcoffee.jfif', 's', 1),
(13, 'capuchino', 3, 'assets/images/cupofcoffee.jfif', 'm', 1),
(14, 'capuchino', 3.5, 'assets/images/cupofcoffee.jfif', 'l', 1),
(15, 'mocha', 3, 'assets/images/cupofcoffee.jfif', 's', 1),
(16, 'mocha', 3.5, 'assets/images/cupofcoffee.jfif', 'm', 1),
(17, 'caramel', 3, 'assets/images/cupofcoffee.jfif', 's', 1),
(18, 'caramel', 3.5, 'assets/images/cupofcoffee.jfif', 'm', 1),
(19, 'latte', 3, 'assets/images/cupofcoffee.jfif', 'm', 1),
(20, 'latte', 3.5, 'assets/images/cupofcoffee.jfif', 'l', 1),
(21, 'black tea', 1.85, 'assets/images/cupofcoffee.jfif', 'm', 2),
(22, 'green tea', 1.85, 'assets/images/cupofcoffee.jfif', 'm', 2),
(23, 'green tea machiato', 2.5, 'assets/images/cupofcoffee.jfif', 'm', 2),
(24, 'peach tea', 2, 'assets/images/cupofcoffee.jfif', 'm', 2),
(25, 'peach tea', 2.5, 'assets/images/cupofcoffee.jfif', 'l', 2),
(26, 'blueberry smoothie', 2.5, 'assets/images/cupofcoffee.jfif', 'm', 3),
(27, 'strawberry smoothie', 2.5, 'assets/images/cupofcoffee.jfif', 'm', 3),
(28, 'cookie cream', 2.75, 'assets/images/cupofcoffee.jfif', 'm', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `promoting`
--

CREATE TABLE `promoting` (
  `ProductID` int(3) NOT NULL,
  `PromotionID` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `promotion`
--

CREATE TABLE `promotion` (
  `ID` int(3) NOT NULL,
  `Description` varchar(512) NOT NULL,
  `BeginDate` date NOT NULL,
  `EndDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `staff`
--

CREATE TABLE `staff` (
  `ID` int(4) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Role` varchar(20) NOT NULL,
  `Status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `StaffID` (`StaffID`),
  ADD KEY `MemberID` (`MemberID`);

--
-- Chỉ mục cho bảng `billdetail`
--
ALTER TABLE `billdetail`
  ADD PRIMARY KEY (`No`),
  ADD KEY `BillID` (`BillID`),
  ADD KEY `ProductID` (`ProductID`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `CategoryID` (`CategoryID`);

--
-- Chỉ mục cho bảng `promoting`
--
ALTER TABLE `promoting`
  ADD PRIMARY KEY (`ProductID`,`PromotionID`),
  ADD KEY `PromotionID` (`PromotionID`);

--
-- Chỉ mục cho bảng `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`ID`);

--
-- Chỉ mục cho bảng `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bill`
--
ALTER TABLE `bill`
  MODIFY `ID` int(8) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `billdetail`
--
ALTER TABLE `billdetail`
  MODIFY `No` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `member`
--
ALTER TABLE `member`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT cho bảng `promotion`
--
ALTER TABLE `promotion`
  MODIFY `ID` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `staff`
--
ALTER TABLE `staff`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `bill`
--
ALTER TABLE `bill`
  ADD CONSTRAINT `bill_ibfk_1` FOREIGN KEY (`StaffID`) REFERENCES `staff` (`ID`),
  ADD CONSTRAINT `bill_ibfk_2` FOREIGN KEY (`MemberID`) REFERENCES `member` (`ID`);

--
-- Các ràng buộc cho bảng `billdetail`
--
ALTER TABLE `billdetail`
  ADD CONSTRAINT `billdetail_ibfk_1` FOREIGN KEY (`BillID`) REFERENCES `bill` (`ID`),
  ADD CONSTRAINT `billdetail_ibfk_2` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ID`);

--
-- Các ràng buộc cho bảng `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`CategoryID`) REFERENCES `category` (`ID`);

--
-- Các ràng buộc cho bảng `promoting`
--
ALTER TABLE `promoting`
  ADD CONSTRAINT `promoting_ibfk_1` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ID`),
  ADD CONSTRAINT `promoting_ibfk_2` FOREIGN KEY (`PromotionID`) REFERENCES `promotion` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
