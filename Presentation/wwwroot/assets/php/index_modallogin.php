<!-- Modal -->
			<div class="modal fade" id="Modal-Login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			  aria-hidden="true">
			  <div class="modal-dialog form-dark" role="document">
			    <!--Content-->
			    <div class="modal-content card card-image" style="background-image: url(assets/images/loginbackground.jpg); ">
			      <div class="text-white rgba-stylish-strong py-5 px-5 z-depth-4">
			        <!--Header-->
			        <div class="modal-header text-center pb-4">
			          <h3 class="modal-title w-100 white-text font-weight-bold" id="myModalLabel"><strong></strong> <a
			              class="green-text font-weight-bold"><strong style="color: white">LOG IN</strong></a></h3>
			          <button type="button" id="close-login" class="close white-text" data-dismiss="modal" aria-label="Close">
			            <span aria-hidden="true">&times;</span>
			          </button>
			        </div>
			        <!--Body-->
			        <form method="POST">
			        <div class="modal-body">
			          <!--Body-->
			          <div class="md-form mb-5">
			            <input type="text" id="loginusername" class="form-control validate white-text" placeholder="Username">
			            <div id="Error_Username_Login"></div>
			          </div>

			          <div class="md-form pb-3">
			            <input type="password" id="loginpassword" class="form-control validate white-text" placeholder="Password">
			            <div id="Error_Password"></div>
			          </div>

			          <!--Grid row-->
			          <div class="row d-flex align-items-center mb-4">

			            <!--Grid column-->
			            <div class="text-center mb-3 col-md-12">
			              <button type="button" id="Login" class="btn btn-success btn-block btn-rounded z-depth-1">Login</button>
			            </div>
			            <!--Grid column-->

			          </div>
			          <!--Grid row-->

			          <!--Grid row-->
			          <div class="row">

			            <!--Grid column-->
			            <div class="col-md-12">
			              <p class="font-small white-text d-flex justify-content-end">Have an account? <a href="#" class="green-text ml-1 font-weight-bold">
			                  Log in</a></p>
			            </div>
			            <!--Grid column-->

			          </div>
			          <!--Grid row-->

			        </div>
			        </form>
			      </div>
			    </div>
			    <!--/.Content-->
			  </div>
			</div>
			<!-- Modal -->