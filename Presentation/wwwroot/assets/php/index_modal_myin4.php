<!-- Modal -->
<div class="modal fade" id="Modal-MyInformation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle" style="color: black">MY IN4</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" style="color: black">
          <div class="form-group" >
            <label class="control-label col-sm-2" for="email">UserName:</label>
            <div class="col-sm-10">
            <?php
              echo  '<input class="form-control" id="email"  value="'.$_SESSION['member']['Username'].'" disabled' ;
            ?>
            </div>
          </div>
          <div class="form-group" >
            <label class="control-label col-sm-2" for="email">Name:</label>
            <div class="col-sm-10">
            <?php
            echo  '<input class="form-control" id="email"  value="'.$_SESSION['member']['Name'].'" disabled' ;
            ?>
            </div>
          </div>
          <div class="form-group" >
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
            <?php
            echo  '<input class="form-control" id="email"  value="'.$_SESSION['member']['Email'].'" disabled' ;
            ?>
            </div>
          </div>
          <div class="form-group" >
            <label class="control-label col-sm-2" for="email">Address:</label>
            <div class="col-sm-10">
            <?php
            echo  '<input  class="form-control" id="NewAddress"  value="'.$_SESSION['member']['Address'].'" ' ;
            ?>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="pwd">Phone:</label>
            <div class="col-sm-10">          
            <?php
            echo  '<input  class="form-control" id="NewPhone"  value="'.$_SESSION['member']['PhoneNumber'].'" ' ;
            ?>
            </div>
          </div>
          <div id="dizz" style="display: none">
          <div class="form-group">
            <label class="control-label col-sm-5" for="pwd" >Current Password:</label>
            <div class="col-sm-10">          
            <?php
            echo  '<input  class="form-control" id="CurrentPwd"' ;
            ?>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-5" for="pwd" >New Password:</label>
            <div class="col-sm-10">          
            <?php
            echo  '<input  class="form-control" id="NewPwd"' ;
            ?>
            </div>
          </div>
        </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="changePassword()" >Change Password</button>
        <button type="button" class="btn btn-primary" id="Myin4">Save changes</button>
      </div>
    </div>
  </div>
</div>