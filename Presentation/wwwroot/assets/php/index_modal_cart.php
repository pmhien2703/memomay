<!-- Modal -->
<div class="modal fade" id="Cart-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle" style="color: black">Shopping cart</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btn-close-cart">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body-cart" style="color: black">
        <?php
        include_once "DataProvider.php";
          if(empty($_SESSION['cart']) || $_SESSION['cart'] ==null)
                    {
                    }
                else
                    {
                      echo '<table class="table">
                                <thead class="thead-dark">
                                  <tr style="text-align: center;">
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Size</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Remove</th>
                                  </tr>
                                </thead>
                                <tbody>';
                                $i=0;
                                $total=0;
                            foreach ($_SESSION['cart'] as $key) {
                                  echo '<tr style="text-align: center;">
                                        <th scope="row">'.++$i.'</th>
                                        <td>'.$key['Name'].'</td>
                                        <td>'.$key['Size'].'</td>
                                        <td>
                                        <form>
                                        <div class="form-group">
                                          <input type="number" class="number-of-product" name="quantity" min="1" max="10" value="'.$key['Quantity'].'" Id="'.$key['ID'].'">
                                          </select>
                                        </div>
                                        </form></td>
                                        <td>'.$key['Price'].'</td>
                                        <td><i class="fas fa-times-circle" style="cursor: pointer;font-size:20px" Id="'.$key['ID'].'"></i></td>
                                      </tr>';
                                  $total=$total+$key['Price']*$key['Quantity'];
                                }
                                  echo '<tr style="text-align: center;">
                                        <th scope="row"></th>
                                        <td></td>
                                        <td></td>
                                        <td style="text-align: right;">TOTAL:</td>
                                        <td><div id="Total">'.$total.'</div></td>
                                        <td></td>
                                      </tr>';  
                      echo '</tbody>
                            </table>';
                    }
        ?>
      </div>
      <div class="modal-footer" id="modal-footer-cart">
        <button type="button" class="btn btn-secondary Remove-All">Delete Cart</button>
        <?php
        if(isset($_SESSION['cart']) || !empty($_SESSION['cart']) )
         { echo
                 "<button type\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" data-target=\"#Delivery-Modal\" id=\"Open-delivery\">Pay</button>";}
        ?>
      </div>
    </div>
  </div>
</div>