<!-- Header Area Starts -->
	<header class="header-area" id="header-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="logo-area">
                        <a href="index.html"><img src="assets/images/logo/logo.png" alt="logo"></a>
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="custom-navbar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>  
                    <div class="main-menu">
                        <ul>
                            <li class="active"><a href="index.php">home</a></li>
                            <li><a href="about.html">about</a></li>
                            <li><a href="menu.html">menu</a></li>
                            <li><a href="#">blog</a>
                                <ul class="sub-menu">
                                    <li><a href="blog-home.html">Blog Home</a></li>
                                    <li><a href="blog-details.html">Blog Details</a></li>
                                </ul>
                            </li>
                            <li><a href="contact-us.html">contact</a></li>
                            <li><a href="elements.html">Elements</a></li>
                            <?php
                                if (isset($_SESSION['member'])) {
                                    $usrName = $_SESSION['member']['Name'];
                            ?>
                            <li><a href="">Welcome <?php echo $usrName;?></a>
                                <ul class="sub-menu">
                                    <li style="cursor: pointer;" data-toggle="modal" data-target="#Modal-MyInformation">My Information</li>
                                    <li style="cursor: pointer;" data-toggle="modal" data-target="#Modal-MyOrder">My Order</li>
                                </ul>
                            </li>
                            <li id="Logout">Log out</li>
                            <?php
                                }
                                else {
                            ?>        
                            <li data-toggle="modal" data-target="#Modal-Login"><a>Login</a></li>
                            <li data-toggle="modal" data-target="#Modal-Signin">Register</li>
                            <?php
                                }
                            ?>    
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Area End -->

    <!-- Banner Area Starts -->
    <section class="banner-area text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 style="margin:0 0 100px 200px; ">Need a space to <span class="prime-color">work</span>?<br>  
                    <span class="style-change">Come here, we'll give you <span class="prime-color"> one.</span></span></h1>
                </div>
            </div>
        </div>
    </section>
    <!-- Banner Area End -->