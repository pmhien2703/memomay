function CheckLoginUsernameRegex() {
    debugger;
    let username_pattern = /^\w{8,16}$/;
    let username_value = $("#login_username").val();

    if (username_value == "") {
        document.getElementById("#login_username_text").innerText("Please enter username");
        document.getElementById("#login_username_text").setAttribute("isValid", "false");
    }
    else if (!username_pattern.test(username_value)) {
        document.getElementById("#login_username_text").innerText("Invalid Username");
        document.getElementById("#login_username_text").setAttribute("isValid", "false");
    }
    else
        document.getElementById("#login_username_text").setAttribute("isValid", "true");
}



function CheckLoginPasswordRegex() {
    debugger;
    let password_pattern = /^\w{8,16}$/;
    let password_value = $("#login_password").val();

    if (username_value == "") {
        document.getElementById("#login_password_text").innerText("Please enter password");
        document.getElementById("#login_password_text").setAttribute("isValid", "false");
    }
    else if (!username_pattern.test(password_value)) {
        document.getElementById("#login_password_text").innerText("Invalid password");
        document.getElementById("#login_password_text").setAttribute("isValid", "false");
    }
    else
        document.getElementById("#login_password_text").setAttribute("isValid", "true");
}



function SendBackLoginData() {
    debugger;
    let username_validation = document.getElementById("#login_username_text").getAttribute("isValid");
    let password_validation = document.getElementById("#login_password_text").getAttribute("isValid");
    if (username_validation == "false" && password_validation == "false") return;

    let username_value = $("#login_username").val();
    let password_value = $("#login_password").val();

    $.ajax({
        url   : "asset/js/login.php",
        method: "POST",
        data  : {
            username: username_value,
            password: password_value
        }
    });
}