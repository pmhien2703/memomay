<!-- Modal -->
			<div class="modal fade" id="Modal-Signin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
			  aria-hidden="true">
			  <div class="modal-dialog form-dark" role="document">
			    <!--Content-->
			    <div class="modal-content card card-image" style="background-image: url(assets/images/loginbackground.jpg); ">
			      <div class="text-white rgba-stylish-strong py-5 px-5 z-depth-4">
			        <!--Header-->
			        <div class="modal-header text-center pb-4">
			          <h3 class="modal-title w-100 white-text font-weight-bold" id="myModalLabel"><strong></strong> <a
			              class="green-text font-weight-bold"><strong style="color: white">SIGN UP</strong></a></h3>
			          <button type="button" class="close white-text" data-dismiss="modal" aria-label="Close">
			            <span aria-hidden="true">&times;</span>
			          </button>
			        </div>
			        <!--Body-->
			        <form method="POST">
			        <div class="modal-body">
			          <!--Body-->
			          <div class="md-form mb-3">
			            <input type="text" id="Username" name="Username" class="form-control validate white-text" placeholder="Username">
			            <div id="Error_Username"></div>
			          </div>

			          <div class="md-form pb-3">
			            <input type="password" id="Password" name="Password" class="form-control validate white-text" placeholder="Password">
			            <div id="Error_Password"></div>
			          </div>

			          <div class="md-form pb-3">
			            <input type="password" id="Confirm_Password" class="form-control validate white-text" placeholder="Confirm Password">
			            <div id="Error_Confirm_Password"></div>
			          </div>

			          <div class="md-form pb-3">
			            <input type="text" id="FullName" name="FullName" class="form-control validate white-text" placeholder="Full Name">
			            <div id="Error_FullName"></div>
			          </div>

			          <div class="md-form pb-3">
			            <input type="email" id="Email" name="Email" class="form-control validate white-text" placeholder="Email">
			            <div id="Error_Email"></div>
			          </div>

			          <div class="md-form pb-3">
			            <input type="text" id="Address" name="Address" class="form-control validate white-text" placeholder="Address">
			            <div id="Error_Address"></div>
			          </div>

			          <div class="md-form pb-3">
			            <input type="text" id="Phone" name="Phone" class="form-control validate white-text" placeholder="Phone">
			            <div id="Error_Phone"></div>
			          </div>

			          <!--Grid row-->
			          <div class="row d-flex align-items-center mb-4">

			            <!--Grid column-->
			            <div class="text-center mb-3 col-md-12">
			              <input type="submit" class="btn btn-success btn-block btn-rounded z-depth-1" value="Submit" id="Signin">
			            </div>
			            <!--Grid column-->

			          </div>
			          <!--Grid row-->

			          <!--Grid row-->
			          <div class="row">
			          </div>
			          <!--Grid row-->

			        </div>
			        </form>
			      </div>
			    </div>
			    <!--/.Content-->
			  </div>
			</div>
			<!-- Modal -->