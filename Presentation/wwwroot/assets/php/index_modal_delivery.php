<!-- Modal -->
<div class="modal fade" id="Delivery-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle" style="color: black">Delivery NOW</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body-delivery">
        <?php
        include_once "DataProvider.php";
          if(empty($_SESSION['cart']) || $_SESSION['cart'] ==null)
                    {
                    }
                else
                    {
                      echo '<table class="table">
                                <thead class="thead-dark">
                                  <tr style="text-align: center;">
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Size</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Remove</th>
                                  </tr>
                                </thead>
                                <tbody>';
                                $i=0;
                                $total=0;
                            foreach ($_SESSION['cart'] as $key) {
                                  echo '<tr style="text-align: center;">
                                        <th scope="row">'.++$i.'</th>
                                        <td>'.$key['Name'].'</td>
                                        <td>'.$key['Size'].'</td>
                                        <td>'.$key['Quantity'].'</td>
                                        <td>'.$key['Price'].'</td>
                                        <td><i class="fas fa-times-circle" style="cursor: pointer;font-size:20px" Id="'.$key['ID'].'"></i></td>
                                      </tr>';
                                  $total=$total+$key['Price']*$key['Quantity'];
                                }
                                  echo '<tr style="text-align: center;">
                                        <th scope="row"></th>
                                        <td></td>
                                        <td></td>
                                        <td style="text-align: right;">TOTAL:</td>
                                        <td>'.$total.'</td>
                                        <td></td>
                                      </tr>';  
                      echo '</tbody>
                            </table>';
                    }
        ?>
  <?php
  if(!empty($_SESSION['member']))
    {echo '
      <form class="needs-validation" id="Form-Delivery" method="POST" style="color: black">
               <div class="form-row">
        <div class="col-md-6 mb-4">
          <label for="validationTooltip01">Full name</label>
          <input type="text" class="form-control" id="Name-Delivery" placeholder="Full Name" value="'.$_SESSION['member']['Name'].'" required>
          <div class="valid-tooltip">
            Looks good!
          </div>
          <div id="Error-FullName-Delivery">
          </div>
        </div>
        <div class="col-md-6 mb-4">
          <label for="validationTooltip02">Phone Number</label>
          <input type="text" class="form-control" id="Phone-Delivery" placeholder="Phone Number" value="'.$_SESSION['member']['PhoneNumber'].'" required>
          <div class="valid-tooltip">
            Looks good!
          </div>
          <div id="Error-Phone-Delivery">
          </div>
        </div>
        <div class="col-md-12 mb-4">
          <label for="validationTooltipUsername">Address</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" id="validationTooltipUsernamePrepend"><i class="fas fa-map-marker-alt"></i></span>
            </div>
            <input type="text" class="form-control" id="Address-Delivery" placeholder="Address" aria-describedby="Address-Delivery" value="'.$_SESSION['member']['Address'].'" required>
            <div class="invalid-tooltip">
              Please choose a unique and valid username.
            </div>
          </div>
        </div>
      </div>
      <h3></h3>
      <div id="ID-Member" IDMember="'.$_SESSION['member']['ID'].'" >
      </div>';}
  else {
        echo '
     <form class="needs-validation" method="POST" id="Form-Delivery" style="color: black">
               <div class="form-row">
        <div class="col-md-6 mb-4">
          <label for="validationTooltip01">Full name</label>
          <input type="text" class="form-control" id="Name-Delivery" placeholder="Full Name" value="" required>
          <div class="valid-tooltip">
            Looks good!
          </div>
          <div id="Error-FullName-Delivery">
          </div>
        </div>
        <div class="col-md-6 mb-4">
          <label for="validationTooltip02">Phone Number</label>
          <input type="text" class="form-control" id="Phone-Delivery" placeholder="Phone Number" value="" required>
          <div class="valid-tooltip">
            Looks good!
          </div>
          <div id="Error-Phone-Delivery">
          </div>
        </div>
        <div class="col-md-12 mb-4">
          <label for="validationTooltipUsername">Address</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" id="validationTooltipUsernamePrepend"><i class="fas fa-map-marker-alt"></i></span>
            </div>
            <input type="text" class="form-control" id="Address-Delivery" placeholder="Address" aria-describedby="Address-Delivery" value="" required>
            <div class="invalid-tooltip">
              Please choose a unique and valid username.
            </div>
          </div>
        </div>
        <div id="Error-Address-Delivery">
        </div>
      </div>
      <h3></h3>
      <div id="ID-Member" IDMember="0" >';
  }
        ?>
      <label for="validationTooltip04">Methods of payment</label>
      <div class="radio">
        <label><input type="radio" name="radio" checked value="COD">Ship COD</label>
      </div>
      <div class="radio">
        <label><input type="radio" name="radio" value="Transfer">Transfer</label>
      </div>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit" id="btn-submit-delivery">Submit form</button>
      </div>
    </div>
  </div>
</div>
