//check in modal sign in
function Username() 
{
	var patternName= /^\w{8,16}$/;
	var username = $('#Username').val();
	document.getElementById('Error_Username').innerText = '';
	if(username=='')
		{	
			document.getElementById('Error_Username').innerText = 'Please Enter Username.';
		}
	else if(!patternName.test(username))
		{
			document.getElementById('Error_Username').innerText = 'Invalid username (8 to 16 characters in length, contains no special characters) Ex:pmhien2703.';
		} 
	else 
		{  
	        $.ajax({  
	             url:"assets/js/check_from.php",  
	             method:"POST",  
	             data:{username:username},  
	             success:function(data){  
	                   if(data!=0)
		                   {
		                   	document.getElementById('Error_Username').innerText = 'Username already exist.Please try again.';
		                   }
	             }  
	        });  
	    }
}

function UsernameLogin() 
{
	var patternName= /^\w{8,16}$/;
	var username = $('#loginusername').val();
	if(username=='')
		{	
			return false;
		}
	else if(!patternName.test(username))
		{
			return false;
		}
	return true;
}

function Usernamet(data) 
{
	var patternName= /^\w{8,16}$/;
	var username = $('#Username').val();
	if(username=='')
		{
			return false;
		}
	else if(!patternName.test(username))
		{
			return false;
		} 
	else if(data!=0)
       {
        return false;
       }
    else
    	{
    	return true;
    	} 
}


function Password() {
	var patternPwd= /^\w{8,16}$/;
	var pwd = $('#Password').val();
	document.getElementById('Error_Password').innerText = '';
	if(pwd=='')
		{
			document.getElementById('Error_Password').innerText = 'Please Enter Password.';
		}
	else if(!patternPwd.test(pwd))
		{
			document.getElementById('Error_Password').innerText = 'Invalid Password (8 to 16 characters in length, contains no special characters).';
		}
}

function Passwordt() {
	var patternPwd= /^\w{8,16}$/;
	var pwd = $('#Password').val();
	if(pwd=='')
		{
			return false;
		}
	else if(!patternPwd.test(pwd))
		{
			return false;
		}
	else return true;
}

function Confirm_Password() {
	var patternCnPwd= /^\w{8,16}$/;
	var cfpwd = $('#Confirm_Password').val();
	document.getElementById('Error_Confirm_Password').innerText = '';
	if(cfpwd=='')
		{
			document.getElementById('Error_Confirm_Password').innerText = 'Please Enter Confirm-Password.';
		}
	else if(!patternCnPwd.test(cfpwd))
		{
			document.getElementById('Error_Confirm_Password').innerText = 'Invalid Confirm-Password (8 to 16 characters in length, contains no special characters).';
		}
	else if(cfpwd != $('#Password').val())
		{
			document.getElementById('Error_Confirm_Password').innerText = 'The password entered does not match.';
		}
}

function Confirm_Passwordt() {
	var patternCnPwd= /^\w{8,16}$/;
	var cfpwd = $('#Confirm_Password').val();
	document.getElementById('Error_Confirm_Password').innerText = '';
	if(cfpwd=='')
		{
			return false;
		}
	else if(!patternCnPwd.test(cfpwd))
		{
			return false;
		}
	else if(cfpwd != $('#Password').val())
		{
			return false;
		}
	else return true;
}

function FullName() {
	var patternfullname= /[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý\s]{2,}$/;
	var fullname = $('#FullName').val();
	document.getElementById('Error_FullName').innerText = '';
	if(fullname=='')
		{
			document.getElementById('Error_FullName').innerText = 'Please Enter Your FullName.';
		}
	else if(!patternfullname.test(fullname))
		{
			document.getElementById('Error_FullName').innerText = 'Invalid Name.';
		}
}

function FullNamet() {
	var patternfullname= /[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý\s]{2,}$/;
	var fullname = $('#FullName').val();
	if(fullname=='')
		{
			return false;
		}
	else if(!patternfullname.test(fullname))
		{
			return false;
		}
	else return true;
}

function Email() {
	var patternemail= /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/;
	var email = $('#Email').val();
	document.getElementById('Error_Email').innerText = '';
	if(email=='')
		{
			document.getElementById('Error_Email').innerText = 'Please Enter Email.';
		}
	else if(!patternemail.test(email))
		{
			document.getElementById('Error_Email').innerText = 'Invalid Email.';
		}
		$.ajax({  
             url:"assets/js/check_from.php",  
             method:"POST",  
             data:{email:email},  
             success:function(data){  
                   if(data!=0)
                   {
                   	document.getElementById('Error_Email').innerText = 'Email already exist.Please try again.';
                   }
             }  
        });
}

function Emailt(data) {
	var patternemail= /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/;
	var email = $('#Email').val();
	if(email=='')
		{
			return false;
		}
	else if(!patternemail.test(email))
		{
			return false;
		}
	else if(data!=0)
       {
        return false;
       }
    else
    	{
    	return true;
    	} 
}

function Phone() {
	var patternPhone=/(09|01[2|6|8|9])+([0-9]{8})\b/;
	var phone = $('#Phone').val();
	document.getElementById('Error_Phone').innerText = '';
	if(phone=='')
		{
			document.getElementById('Error_Phone').innerText = 'Please Enter Phone number.';
			return false;
		}
	else if(!patternPhone.test(phone))
		{
			document.getElementById('Error_Phone').innerText = 'Invalid Phone.';
			return false;
		}
}

function Phonet() {
	var patternPhone=/(09|01[2|6|8|9])+([0-9]{8})\b/;
	var phone = $('#Phone').val();
	if(phone=='')
		{
			return false;
		}
	else if(!patternPhone.test(phone))
		{
			return false;
		}
	else return true;
}

function Address() {
	var patternAdd= /[a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý\s]{5,}/;
	var add = $('#Address').val();
	document.getElementById('Error_Address').innerText = '';
	if(add=='')
		{
			document.getElementById('Error_Address').innerText = 'Please Enter Your Address.';
		}
	else if(!patternAdd.test(add))
		{
			document.getElementById('Error_Address').innerText = 'Invalid Phone.';
		}
}

function Addresst() {
	var patternAdd= /[a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý\s]{5,}/;
	var add = $('#Address').val();
	if(add=='')
		{
			return false;
		}
	else if(!patternAdd.test(add))
		{
			return false;
		}
	else return true;
}
//check in modal cart
function Fullname_Cart()
{
	var patternfullname= /[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý\s]{2,}$/;
	var fullname = $('#Name-Delivery').val();
	document.getElementById('Error-FullName-Delivery').innerText = '';
	if(fullname=='')
		{
			document.getElementById('Error-FullName-Delivery').innerText = 'Please Enter Your FullName.';
		}
	else if(!patternfullname.test(fullname))
		{
			document.getElementById('Error-FullName-Delivery').innerText = 'Invalid Name.';
		}
}

function Address_Cart() {
	var patternAdd= /[a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹý\s]{5,}/;
	var add = $('#Address-Delivery').val();
	document.getElementById('Error-Address-Delivery').innerText = '';
	if(add=='')
		{
			document.getElementById('Error-Address-Delivery').innerText = 'Please Enter Your Address.';
		}
	else if(!patternAdd.test(add))
		{
			document.getElementById('Error-Address-Delivery').innerText = 'Invalid Address.';
		}
}

function Phone_Cart() {
	var patternPhone=/(09|01[2|6|8|9])+([0-9]{8})\b/;
	var phone = $('#Phone-Delivery').val();
	document.getElementById('Error-Address-Delivery').innerText = '';

	if(phone=='')
		{
			document.getElementById('Error-Phone-Delivery').innerText = 'Please Enter Your Phone number.';
		}
	else if(!patternPhone.test(add))
		{
			document.getElementById('Error-Phone-Delivery').innerText = 'Invalid Phone.';
		}
}