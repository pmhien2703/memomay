using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure;
using Presentation.ServiceViews;
using Microsoft.AspNetCore.Http;
using ApplicationCore.ViewModels;

namespace Presentation.Controllers
{
    public class ThongKeController : Controller
    {
        private readonly ThongKeServiceView _serviceTKV;
        public ThongKeController(ThongKeServiceView serviceTKV)
        {
            this._serviceTKV = serviceTKV;
        }

        public void SetDate()
        {
            string NowDate = DateTime.Now.ToString("yyyy-MM-dd");
            if(ViewData["From"]==null)
            {
                ViewData["From"] = NowDate;
            }
            if(ViewData["To"] == null)
            {
            ViewData["To"] = NowDate;
            }
        }
        [HttpGet]
        public IActionResult Index(DateTime From,DateTime To,string Choice)
        {
            if (HttpContext.Session.GetString("Username") == null || HttpContext.Session.GetString("VaiTro") == "Member")
            {
                HttpContext.Session.Remove("Username");
                return RedirectToAction("Index", "Account");
            }
            else
            {
                if(Choice == "Đơn hàng")
                {
                    ViewData["From"] = From.ToString("yyyy-MM-dd");
                    ViewData["To"] = To.ToString("yyyy-MM-dd");
                    return RedirectToAction("DonHang", "ThongKe", new {From = From,To = To});
                }
                else if(Choice == "Sản phẩm")
                {
                    ViewData["From"] = From.ToString("yyyy-MM-dd");
                    ViewData["To"] = To.ToString("yyyy-MM-dd");
                    return RedirectToAction("SanPham", "ThongKe", new { From = From, To = To });
                }
                else if(Choice == "Size")
                {
                    ViewData["From"] = From.ToString("yyyy-MM-dd");
                    ViewData["To"] = To.ToString("yyyy-MM-dd");
                    return RedirectToAction("Size", "ThongKe", new { From = From, To = To });
                }
                else if(Choice =="Màu")
                {
                    ViewData["From"] = From.ToString("yyyy-MM-dd");
                    ViewData["To"] = To.ToString("yyyy-MM-dd");
                    return RedirectToAction("Mau", "ThongKe", new { From = From, To = To });
                }
                else
                {
                    SetDate();
                    return View();
                }
            }
        }
        [HttpGet]
        public IActionResult DonHang(DateTime From,DateTime To)
        {
            if (HttpContext.Session.GetString("Username") == null || HttpContext.Session.GetString("VaiTro") == "Member")
            {
                HttpContext.Session.Remove("Username");
                return RedirectToAction("Index", "Account");
            }
            else
            {
                ViewData["From"] = From.ToString("yyyy-MM-dd");
                ViewData["To"] = To.ToString("yyyy-MM-dd");
                ViewData["TitleFrom"] = From.ToString("dd-MM-yyyy");
                ViewData["TitleTo"] = To.ToString("dd-MM-yyyy");
                return View(_serviceTKV.GetThongKeDonHangVM(From, To));
            }
        }
        [HttpGet]
        public IActionResult SanPham(DateTime From ,DateTime To)
        {
            if (HttpContext.Session.GetString("Username") == null || HttpContext.Session.GetString("VaiTro") == "Member")
            {
                HttpContext.Session.Remove("Username");
                return RedirectToAction("Index", "Account");
            }
            else
            {
                ViewData["From"] = From.ToString("yyyy-MM-dd");
                ViewData["To"] = To.ToString("yyyy-MM-dd");
                return View(_serviceTKV.GetThongKeSanPhamVMs(From,To));
            }
        }
        [HttpGet]
        public IActionResult Mau(DateTime From, DateTime To)
        {
            if (HttpContext.Session.GetString("Username") == null || HttpContext.Session.GetString("VaiTro") == "Member")
            {
                HttpContext.Session.Remove("Username");
                return RedirectToAction("Index", "Account");
            }
            else
            {
                ViewData["From"] = From.ToString("yyyy-MM-dd");
                ViewData["To"] = To.ToString("yyyy-MM-dd");
                return View(_serviceTKV.GetThongKeMauVM(From, To));
            }
        }
    }
}
