﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Presentation.Models;
using Microsoft.AspNetCore.Http;
using Presentation.ServiceViews;

namespace Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly HomeServiceView _service;
        public HomeController(HomeServiceView serviceView)
        {
            _service = serviceView;
        }
        public IActionResult Index()
        {
            if(HttpContext.Session.GetString("Username") == null){
                return RedirectToAction("Index", "Account");
            }else{
                return View(_service.GetIndexModel());
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
