using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure;
using ApplicationCore.Services;
using ApplicationCore.Interfaces.IServices;
using Microsoft.AspNetCore.Http;
using ApplicationCore.ViewModels;
using Presentation.ServiceViews;
using System.Web;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Presentation.Controllers
{
    public class SanPhamController : Controller
    {
        private readonly ISanPhamService _service;
        private readonly IBoSuuTapService _serviceBST;
        private readonly SanPhamServiceView _serviceView;
        private readonly IHostingEnvironment hosting;

        public SanPhamController(ISanPhamService service,IBoSuuTapService serviceBST,SanPhamServiceView serviceView,IHostingEnvironment hosting)
        {
            _service = service;
            _serviceBST = serviceBST;
            _serviceView = serviceView;
            this.hosting = hosting;
        }
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                SanPhamIndexVM vm = _serviceView.GetSanPhamIndexVM("", "", 1);
                return View(vm);
            }
        }
        [HttpGet]
        // GET: SanPham
        public IActionResult Index(string CurrentFilter,string sort ,int pageIndex = 1)
        {
            //AddViewData
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                ViewData["CurrentFilter"] = CurrentFilter;
                if(sort == null)
                {
                    ViewData["sort"] = "";
                }
                else
                {
                    ViewData["sort"] = sort;
                }
                SanPhamIndexVM vm = _serviceView.GetSanPhamIndexVM(CurrentFilter,sort, pageIndex);
                return View(vm);
            }
        }

        public IActionResult Create()
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                SanPhamCreateVM vm = new SanPhamCreateVM();
                vm.listBST = _serviceBST.GetAll();
                return View(vm);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(SanPhamCreateVM SanPhamCreateVM, IFormFile file, double Gia1,double Gia2,double Gia3)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                if(_service.CheckValidProduct(SanPhamCreateVM.sanPham.TenSanPham,SanPhamCreateVM.sanPham.Mau))
                {
                    string uniQueFileName = null;
                    if (file != null)
                    {
                        string uploadsFolder = Path.Combine(hosting.WebRootPath, "img");
                        uniQueFileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                        string filePath = Path.Combine(uploadsFolder, uniQueFileName);
                        file.CopyTo(new FileStream(filePath, FileMode.Create));
                        for (int i = 1; i <= 12; i++)
                        {
                            SanPham sp = new SanPham();
                            sp = SanPhamCreateVM.sanPham;
                            sp.TenSanPham = VietHoa(sp.TenSanPham);
                            sp.Mau = VietHoa(sp.Mau);
                            sp.ChatLieu = VietHoa(sp.ChatLieu);
                            if(i>=1 && i<5)
                            {
                                sp.GiaGoc = Gia1;
                            }
                            else if(i>=5 && i<=8)
                            {
                                sp.GiaGoc = Gia2;
                            }
                            else if(i>8 && i<=12)
                            {
                                sp.GiaGoc = Gia3;
                            }
                            sp.MaSanPham = _service.MaSanPham();
                            sp.SoLuongTon = 0;
                            sp.TrangThai = 1;
                            sp.HinhAnh = uniQueFileName;
                            sp.Size = i.ToString();
                            _service.Add(sp);
                        }
                        return RedirectToAction("Index", "SanPham");
                    }
                    else
                    {
                        ViewData["HinhAnh"] = "Vui lòng chọn hình ảnh";
                        SanPhamCreateVM vm = new SanPhamCreateVM();
                        vm.listBST = _serviceBST.GetAll();
                        return View(vm);
                    }
                }
                else
                {
                    ViewData["TenSanPham"] = "Tên sản phẩm bị trùng lặp";
                    SanPhamCreateVM vm = new SanPhamCreateVM();
                    vm.listBST = _serviceBST.GetAll();
                    return View(vm);
                }
            }
        }
        public static string VietHoa(string s)
        {
            if (String.IsNullOrEmpty(s))
                return s;

            string result = "";

            //lấy danh sách các từ  

            string[] words = s.Split(' ');

            foreach (string word in words)
            {
                // từ nào là các khoảng trắng thừa thì bỏ  
                if (word.Trim() != "")
                {
                    if (word.Length > 1)
                        result += word.Substring(0, 1).ToUpper() + word.Substring(1).ToLower() + " ";
                    else
                        result += word.ToUpper() + " ";
                }

            }
            return result.Trim();
        }
        [HttpGet]
        public IActionResult Edit(string id)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                return View(_serviceView.GetSanPhamEditVM(id));
            }
        }
        [HttpPost]
        public IActionResult Edit(SanPhamEditVM SanPhamEditVM, IFormFile file,string TenSanPham)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                if (_service.CheckValidProduct(SanPhamEditVM.sanPham.TenSanPham,SanPhamEditVM.sanPham.Mau) || SanPhamEditVM.sanPham.TenSanPham == TenSanPham)
                {
                    string uniQueFileName = null;
                    if (file != null)
                    {
                        string uploadsFolder = Path.Combine(hosting.WebRootPath, "img");
                        uniQueFileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                        string filePath = Path.Combine(uploadsFolder, uniQueFileName);
                        file.CopyTo(new FileStream(filePath, FileMode.Create));
                    }
                    for (int i = 1; i <= 12; i++)
                    {
                        SanPham sp = _service.GetByNameAndSizeAndColor(SanPhamEditVM.sanPham.TenSanPham,i.ToString(),SanPhamEditVM.sanPham.Mau);
                        switch(Int32.Parse(sp.Size)){
                            case 1 :
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize1;
                                sp.GiaGoc = SanPhamEditVM.Gia1;
                                break;
                            case 2 :
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize1;
                                sp.GiaGoc = SanPhamEditVM.Gia1;
                                break;
                            case 3:
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize3;
                                sp.GiaGoc = SanPhamEditVM.Gia1;
                                break;
                            case 4:
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize4;
                                sp.GiaGoc = SanPhamEditVM.Gia1;
                                break;
                            case 5:
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize5;
                                sp.GiaGoc = SanPhamEditVM.Gia2;
                                break;
                            case 6:
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize6;
                                sp.GiaGoc = SanPhamEditVM.Gia2;
                                break;
                            case 7:
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize7;
                                sp.GiaGoc = SanPhamEditVM.Gia2;
                                break;
                            case 8:
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize8;
                                sp.GiaGoc = SanPhamEditVM.Gia2;
                                break;
                            case 9:
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize9;
                                sp.GiaGoc = SanPhamEditVM.Gia3;
                                break;
                            case 10:
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize10;
                                sp.GiaGoc = SanPhamEditVM.Gia3;
                                break;
                            case 11:
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize11;
                                sp.GiaGoc = SanPhamEditVM.Gia3;
                                break;
                            case 12:
                                sp.SoLuongTon = SanPhamEditVM.hangTonSize12;
                                sp.GiaGoc = SanPhamEditVM.Gia3;
                                break;
                            default:
                            break;

                        }
                        if(uniQueFileName!=null)
                        sp.HinhAnh = uniQueFileName;
                        sp.TenSanPham = VietHoa(SanPhamEditVM.sanPham.TenSanPham);
                        sp.ChatLieu = VietHoa(SanPhamEditVM.sanPham.ChatLieu);
                        sp.GiaBan = SanPhamEditVM.sanPham.GiaBan;
                        sp.MaBoSuuTap = SanPhamEditVM.sanPham.MaBoSuuTap;
                        sp.Mau = VietHoa(SanPhamEditVM.sanPham.Mau);
                        _service.Update(sp);
                    }
                    return RedirectToAction("Index", "SanPham");
                    // }
                    // else
                    // {
                    //     for (int i = 1; i <= 12; i++)
                    //     {
                    //         SanPham sp = _service.GetById(_service.GetIdByNameAndSize(TenSanPham, i.ToString()));
                    //         if (i >= 1 && i <= 4)
                    //         {
                    //             sp.GiaGoc = SanPhamEditVM.Gia1;
                    //         }
                    //         else if (i >= 5 && i <= 8)
                    //         {
                    //             sp.GiaGoc = SanPhamEditVM.Gia2;
                    //         }
                    //         else if (i >= 9 && i <= 12)
                    //         {
                    //             sp.GiaGoc = SanPhamEditVM.Gia3;
                    //         }
                    //         sp.TenSanPham = SanPhamEditVM.sanPham.TenSanPham;
                    //         sp.ChatLieu = SanPhamEditVM.sanPham.ChatLieu;
                    //         sp.GiaBan = SanPhamEditVM.sanPham.GiaBan;
                    //         sp.MaBoSuuTap = SanPhamEditVM.sanPham.MaBoSuuTap;
                    //         sp.Mau = SanPhamEditVM.sanPham.Mau;
                    //         _service.Update(sp);
                    //     }
                }
            else
            {
                ViewData["TenSanPham"] = "Tên sản phẩm bị trùng lặp";
                return View(_serviceView.GetSanPhamEditVM(SanPhamEditVM.sanPham.TenSanPham));
            }
        }
        }
        //TODO EDIT POST
        [HttpGet]
        public IActionResult Details(string id,string color)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                return View(_service.GetByNameAndColor(id,color));
            }
        }
        [HttpGet]
        public IActionResult Remove(string id)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                _service.RemoveByName(id);
                return RedirectToAction(nameof(Index));
            }
        }
    }
}