using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure;
using ApplicationCore.Interfaces.IServices;
using Microsoft.AspNetCore.Http;
using Presentation.ServiceViews;
using ApplicationCore.ViewModels;

namespace Presentation.Controllers
{
    public class ThongTinNhapHangController : Controller
    {
        public static List<ChiTietNhapHangVM> listtam = null;
        private readonly IThongTinNhapHangService _serviceTTNH;
        private readonly ISanPhamService _serviceSP;
        private readonly ThongTinNhapHangServiceView _serviceTTNHV;
        public ThongTinNhapHangController (IThongTinNhapHangService serviceTTNH,ISanPhamService serviceSP,ThongTinNhapHangServiceView serviceTTNHV)
        {
            _serviceTTNH = serviceTTNH;
            _serviceSP = serviceSP;
            _serviceTTNHV = serviceTTNHV;
        }
        public IActionResult Index()
        {
            if(HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                return View(_serviceTTNHV.GetThongTinNhapHangIndex(1));
            }
        }
        [HttpPost]
        public IActionResult Index(int pageIndex=1)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                return View(_serviceTTNHV.GetThongTinNhapHangIndex(pageIndex));
            }
        }
        public IActionResult Create()
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                if (listtam == null)
                {
                    listtam = new List<ChiTietNhapHangVM>();
                }
                ThongTinNhapHangCreateVM CreateVM = new ThongTinNhapHangCreateVM
                {
                    SanPhams = _serviceSP.GetAllName(),
                    ListChiTietNhapHangs = listtam,
                    sanPham = new SanPham()
                };
                return View(CreateVM);
            }
        }
        [HttpPost]
        [AutoValidateAntiforgeryToken]
        public IActionResult Create(ThongTinNhapHangCreateVM vm)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                ThongTinNhapHangCreateVM newvm = _serviceTTNHV.CreateVM(vm);
                return View(newvm);
            }
        }
        [HttpPost]
        public IActionResult Remove(int i)
        {
            if (HttpContext.Session.GetString("Username") != null)
            {
                listtam.RemoveAt(i);
                return RedirectToAction("Create", "ThongTinNhapHang");
            }
            else
            {
                return RedirectToAction("Index", "Account");
            }
        }
        [HttpPost]
        public IActionResult CreateTTNH(ThongTinNhapHangCreateVM vm)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                int idNV = Int32.Parse(HttpContext.Session.GetString("MaNhanVien"));
                _serviceTTNHV.CreateTTNH(vm,idNV);
                return RedirectToAction("Index", "ThongTinNhapHang");
            }
        }
        [HttpGet]
        public IActionResult Details(int id)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                return View(_serviceTTNHV.Details(id));
            }
        }
    }
}
