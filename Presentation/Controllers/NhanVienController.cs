using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure;
using ApplicationCore.Interfaces.IServices;
using Microsoft.AspNetCore.Http;
using System.Security.Cryptography;

namespace Presentation.Controllers
{
    public class NhanVienController : Controller
    {
        private readonly INhanVienService _serviceNV;
        public NhanVienController(INhanVienService service)
        {
            _serviceNV = service;
        }
        public IActionResult Index()
        {
            if(HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                if(HttpContext.Session.GetString("VaiTro") == "SuperAdmin")
                {
                    return View(_serviceNV.Getall());
                }
                else if(HttpContext.Session.GetString("VaiTro") == "Admin")
                {
                    return View(_serviceNV.Getall().Where(m => m.VaiTro == "Member"));
                }
                else
                {
                    HttpContext.Session.Remove("Username");
                    return RedirectToAction("Index", "Account");
                }
            }
        }
        [HttpPost]
        public bool ChangeStatus(int id, int status)
        {
            if (HttpContext.Session.GetString("Username") == null)
                return false;
            NhanVien nv = _serviceNV.GetById(id);
            nv.TrangThai = status;
            _serviceNV.Update(nv);
            return true;
        }
        [HttpPost]
        public bool ChangeRole(int id, string role)
        {
            if (HttpContext.Session.GetString("Username") == null)
                return false;
            NhanVien nv = _serviceNV.GetById(id);
            nv.VaiTro = role;
            _serviceNV.Update(nv);
            return true;
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                return View(_serviceNV.GetById(id));
            }
        }
        [HttpPost]
        public IActionResult Edit(NhanVien NhanVien,string MatKhauXacNhan,string MatKhau)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                if(MatKhau != MatKhauXacNhan)
                {
                    ViewData["LoiMatKhau"] = "Mật khẩu không khớp";
                    return View(_serviceNV.GetById(NhanVien.MaNhanVien));
                }
                else
                {
                    NhanVien nv = _serviceNV.GetById(NhanVien.MaNhanVien);
                    nv.HoTen = NhanVien.HoTen;
                    nv.SDT = NhanVien.SDT;
                    nv.GioiTinh = NhanVien.GioiTinh;
                    using(MD5 md5hash = MD5.Create())
                    {
                        nv.MatKhau = _serviceNV.GetMd5hash(md5hash, MatKhauXacNhan);
                    }
                    _serviceNV.Update(nv);
                    ViewData["ThongBaoThanhCong"] = "Cập nhật thông tin thành công";
                    return View(_serviceNV.GetById(NhanVien.MaNhanVien));
                }
                
            }
        }
    }
}
