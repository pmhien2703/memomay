using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure;
using ApplicationCore.Interfaces.IServices;
using ApplicationCore.ViewModels;
using Microsoft.AspNetCore.Http;
using Presentation.ServiceViews;
namespace Presentation.Controllers
{
    public class DonHangController : Controller
    {
        //public static listtam duoc gan o DonHangServiceView
        public static List<ThongTinChiTietDonHangVM> listtam = null;
        private IDonHangService _serviceDH;
        private ISanPhamService _serviceSP;
        private IKhachHangService _serviceKH;
        private DonHangServiceView _serviceDHV;
        public DonHangController(IDonHangService serviceDH,ISanPhamService sanPhamService,IKhachHangService khachHangService, DonHangServiceView serviceView)
        {
            _serviceDH = serviceDH;
            _serviceKH = khachHangService;
            _serviceSP = sanPhamService;
            _serviceDHV = serviceView;
        }
        // GET: DonHang
        public IActionResult Index()
        {
            if(HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                var vm = _serviceDH.GetAll();
                return View(vm);
            }
        }
        [HttpGet]
        public IActionResult Index(string sort,int pageIndex =1)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                if(sort == null)
                    ViewData["TinhTrangDonHang"] = "Tất cả";
                else
                    ViewData["TinhTrangDonHang"] = sort;
                return View(_serviceDHV.GetDonHangIndex(sort,pageIndex));
            }
        }
        public IActionResult Create()
        {
            if(HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                if(listtam == null)
                {
                    listtam = new List<ThongTinChiTietDonHangVM>();
                }
                DonHangCreateVM donHangCreateVM = new DonHangCreateVM
                {
                    ThongTinSP = _serviceSP.GetAllName().OrderBy(t=>t.TenSanPham).ToList(),
                    ThongTinKH = _serviceKH.GetAll(),
                    ThongTinCTDH = listtam,
                    ThongTinDH = new DonHang(),
                    TongTien = _serviceDHV.TinhTongTien(listtam),
                };
                //Set Value for Date when role = SuperAdmin
                string NowDate = DateTime.Now.ToString("yyyy-MM-dd");
                ViewData["Ngay"] = NowDate;
                return View(donHangCreateVM);
            }
        }
        [HttpPost]
        public IActionResult Create(DonHangCreateVM DonHangCreateVM)
        {
            DonHangCreateVM.sanPham.Mau = _serviceSP.GetById(DonHangCreateVM.sanPham.MaSanPham).Mau;
            DonHangCreateVM.sanPham.TenSanPham = _serviceSP.GetById(DonHangCreateVM.sanPham.MaSanPham).TenSanPham;

            if(HttpContext.Session.GetString("Username") != null)
            {
                if(!_serviceDH.CheckSizeExist(DonHangCreateVM.sanPham))
                    {
                    ViewData["TenSizeSai"] = "Size không tồn tại";
                    if(listtam == null)
                    {
                        listtam = new List<ThongTinChiTietDonHangVM>();
                    }
                    DonHangCreateVM vm = new DonHangCreateVM
                    {
                        ThongTinSP = _serviceSP.GetAllName(),
                        ThongTinKH = _serviceKH.GetAll(),
                        ThongTinCTDH = listtam,
                        ThongTinDH = new DonHang(),
                        TongTien = _serviceDHV.TinhTongTien(listtam)
                };
                    //Set Value for Date when role = SuperAdmin
                    string NowDate = DateTime.Now.ToString("yyyy-MM-dd");
                    ViewData["Ngay"] = NowDate;
                    return View(vm);
                    }
                else if (!_serviceDH.CheckInventoryNumber(DonHangCreateVM.sanPham.SoLuongTon, _serviceSP.GetByNameAndSizeAndColor(DonHangCreateVM.sanPham.TenSanPham, DonHangCreateVM.sanPham.Size, DonHangCreateVM.sanPham.Mau).MaSanPham))
                {
                    ViewData["KhongDuHangTon"] = "Số lượng tồn không đủ";
                    if (listtam == null)
                    {
                        listtam = new List<ThongTinChiTietDonHangVM>();
                    }
                    DonHangCreateVM vm = new DonHangCreateVM
                    {
                        ThongTinSP = _serviceSP.GetAllName(),
                        ThongTinKH = _serviceKH.GetAll(),
                        ThongTinCTDH = listtam,
                        ThongTinDH = new DonHang(),
                        TongTien = _serviceDHV.TinhTongTien(listtam)
                    };
                    string NowDate = DateTime.Now.ToString("yyyy-MM-dd");
                    ViewData["Ngay"] = NowDate;
                    return View(vm);
                }
                else
                {
                    DonHangCreateVM vm = _serviceDHV.CreateDHVM(DonHangCreateVM);
                    //Set Value for Date when role = SuperAdmin
                    string NowDate = DateTime.Now.ToString("yyyy-MM-dd");
                    ViewData["Ngay"] = NowDate;
                    return View(vm);
                }
            }
            else
            {
                return RedirectToAction("Index", "Account");
            }
        }
        [HttpPost]
        public IActionResult CreateDonHang(DonHangCreateVM DonHangCreateVM)
        {
            if(HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                _serviceDHV.CreateDonHang(DonHangCreateVM);
                return RedirectToAction("Index", "DonHang");
            }
        }
        [HttpPost]
        public IActionResult Remove(int i)
        {
            if(HttpContext.Session.GetString("Username") != null)
            {
                listtam.RemoveAt(i);
                return RedirectToAction("Create", "DonHang");
            }
            else
            {
                return RedirectToAction("Index", "Account");
            }
        }
        [HttpPost]
        public bool ChangeStatus(int id,string status,int pageindex,string sort)
        {
            _serviceDHV.ChangeStatus(id, status, sort, pageindex);
            return true;
        }
        [HttpPost]
        public bool ChangeQuantity(int id,int quantity,int idsanpham)
        {
            return _serviceDHV.ChangeQuantity(id, quantity,idsanpham);
        }
        [HttpGet]
        public IActionResult Details(int id)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                DonHangEditVM vm = _serviceDHV.Detail(id);
                return View(vm);
            }
        }
        [HttpPost]
        [AutoValidateAntiforgeryToken]

        public IActionResult Edit(DonHangEditVM DonHangEditVM)
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                DonHang dh = _serviceDH.GetById(DonHangEditVM.donHang.MaDonHang);
                dh.ChiPhiVanChuyen = DonHangEditVM.donHang.ChiPhiVanChuyen;
                dh.GhiChu = DonHangEditVM.donHang.GhiChu;
                dh.PhuongThucVanChuyen = DonHangEditVM.donHang.PhuongThucVanChuyen;
                _serviceDH.Update(dh);
                return RedirectToAction("Index", "DonHang");
            }
        }
        [HttpPost]
        public bool ChangeSize(int stt, string size,int idsanpham)
        {
            return _serviceDHV.ChangeSize(stt, size, idsanpham);
        }
        [HttpPost]
        public bool RemoveOrderLine(int id)
        {
            return _serviceDHV.RemoveOrderLine(id);
        }
        [HttpPost]
        public bool ChangeDiscount(int id,double discount)
        {
            return _serviceDHV.ChangeDiscount(id,discount);
        }
        [Produces("application/json")]
        [HttpPost]
        public JsonResult AutoCompleteCustomer(string Prefix)
        {
            var data = (from kh in _serviceKH.GetAll().ToList()
                       where kh.TenKhachHang.Contains(Prefix) || kh.SDT.Contains(Prefix)
                       select new { kh.TenKhachHang ,  kh.SDT,kh.MaKhachHang,kh.DiaChi});
            return Json(data);
        }
    }
}
