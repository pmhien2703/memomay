using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure;

namespace Presentation.Controllers
{
    public class BoSuuTapController : Controller
    {
        private readonly MeMoMay _context;

        public BoSuuTapController(MeMoMay context)
        {
            _context = context;
        }

        // GET: BoSuuTap
        public async Task<IActionResult> Index()
        {
            return View(await _context.BoSuuTaps.ToListAsync());
        }

        // GET: BoSuuTap/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var boSuuTap = await _context.BoSuuTaps
                .FirstOrDefaultAsync(m => m.MaBoSuuTap == id);
            if (boSuuTap == null)
            {
                return NotFound();
            }

            return View(boSuuTap);
        }

        // GET: BoSuuTap/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: BoSuuTap/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MaBoSuuTap,TenBoSuuTap")] BoSuuTap boSuuTap)
        {
            if (ModelState.IsValid)
            {
                _context.Add(boSuuTap);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(boSuuTap);
        }

        // GET: BoSuuTap/Edit/5
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var boSuuTap = await _context.BoSuuTaps.FindAsync(id);
            if (boSuuTap == null)
            {
                return NotFound();
            }
            return View(boSuuTap);
        }

        // POST: BoSuuTap/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MaBoSuuTap,TenBoSuuTap")] BoSuuTap boSuuTap)
        {
            if (id != boSuuTap.MaBoSuuTap)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(boSuuTap);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BoSuuTapExists(boSuuTap.MaBoSuuTap))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(boSuuTap);
        }

        // GET: BoSuuTap/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var boSuuTap = await _context.BoSuuTaps
                .FirstOrDefaultAsync(m => m.MaBoSuuTap == id);
            if (boSuuTap == null)
            {
                return NotFound();
            }

            return View(boSuuTap);
        }

        // POST: BoSuuTap/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var boSuuTap = await _context.BoSuuTaps.FindAsync(id);
            _context.BoSuuTaps.Remove(boSuuTap);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BoSuuTapExists(int id)
        {
            return _context.BoSuuTaps.Any(e => e.MaBoSuuTap == id);
        }
    }
}
