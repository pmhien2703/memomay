using ApplicationCore.Interfaces.IServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using Infrastructure;
using Infrastructure.Persistence;
using Microsoft.AspNetCore.Http;
using System.Text.RegularExpressions;

namespace Presentation.Controllers
{
    public class AccountController : Controller
    {
        private INhanVienService _service;
        public AccountController(INhanVienService service)
        {
            _service = service;
        }
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("Username") == null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index([Bind("TenDangNhap", "MatKhau")] NhanVien user)
        {
            string username = user.TenDangNhap;
            string password = user.MatKhau;
            int result = _service.Login(username, password);
            if (result == 2)
            {
                List<string> listSession = _service.CreateSession(username, password);
                HttpContext.Session.SetString("Username", username);
                HttpContext.Session.SetString("TenNhanVien", listSession.ElementAt(0));
                HttpContext.Session.SetString("MaNhanVien", listSession.ElementAt(1));
                HttpContext.Session.SetString("VaiTro", listSession.ElementAt(2));
                HttpContext.Session.SetString("TrangThai", listSession.ElementAt(3));
                return RedirectToAction("Index", "Home");
            }
            else if(result == 0 || result == 3)
            {
                @ViewData["ThongBao"] = "Tên đăng nhập hoặc mật khẩu không hợp lệ";
                return View();
            }
            else
            {
                @ViewData["ThongBao"] = "Tài khoản chưa có quyền hoạt động";
                return View();
            }
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("Username");
            HttpContext.Session.Remove("TenNhanVien");
            HttpContext.Session.Remove("VaiTro");
            HttpContext.Session.Remove("MaNhanVien");
            return RedirectToAction("Index", "Account");
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(NhanVien NhanVien, string MatKhauXacNhan)
        {
            string TenDangNhap = NhanVien.TenDangNhap;
            //check trùng lặp tên đăng nhập
            if(_service.CheckUserName(TenDangNhap))
            {
                //Check số điện thoại trùng lặp
                if(_service.CheckPhoneNumberDuplicated(NhanVien.SDT))
                {
                    //Check khớp mật khẩu
                    if (String.Compare(NhanVien.MatKhau, MatKhauXacNhan, false) == 0)
                    {
                        NhanVien nv = NhanVien;
                        nv.TrangThai = 0;
                        nv.VaiTro = "Member";
                        using (MD5 md5hash = MD5.Create())
                        {
                            nv.MatKhau = _service.GetMd5hash(md5hash, NhanVien.MatKhau);
                        }
                        _service.Add(NhanVien);
                        ViewData["ThongBaoThanhCong"] = "Đăng kí thành công, vui lòng đợi quản trị viên xét duyệt";
                        return View();
                    }
                    else
                    {
                        ViewData["MatKhauXacNhan"] = "Mật khẩu không trùng khớp";
                        return View();
                    }
                }
                else
                {
                    ViewData["SDT"] = "Số điện thoại đã tồn tại";
                    return View();
                }
            }
            else
            {
                //Thong bao trung ten dang nhap
                ViewData["TenDangNhap"] = "Tên đăng nhập trùng lặp";
                return View();
            }
        }
    }
}