using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationCore.Entities;
using Infrastructure;
using ApplicationCore.Interfaces.IServices;
using Presentation.ServiceViews;
using Microsoft.AspNetCore.Http;
using ApplicationCore.ViewModels;

namespace Presentation.Controllers
{
    public class KhachHangController : Controller
    {
        private readonly MeMoMay _context;
        private readonly IKhachHangService _service;
        private readonly KhachHangServiceView _serviceV;

        public KhachHangController(MeMoMay context,IKhachHangService service,KhachHangServiceView serviceView)
        {
            _context = context;
            _service = service;
            _serviceV = serviceView;
        }

        // GET: KhachHang
        public IActionResult Index()
        {
            if(HttpContext.Session.GetString("Username") != null )
            {
                return View(_serviceV.GetHangIndexVM(null,1));
            }
            else
            {
                return RedirectToAction("Index", "Account");
            }
        }
        [HttpGet]
        public IActionResult Index(string CurrentFilter, int pageIndex = 1)
        {
            //AddViewData
            ViewData["CurrentFilter"] = CurrentFilter;
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Index", "Account");
            }
            else
            {
                KhachHangIndexVM vm = _serviceV.GetHangIndexVM(CurrentFilter, pageIndex);
                return View(vm);
            }
        }

        // GET: KhachHang/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khachHang = await _context.KhachHangs
                .FirstOrDefaultAsync(m => m.MaKhachHang == id);
            if (khachHang == null)
            {
                return NotFound();
            }

            return View(khachHang);
        }

        // GET: KhachHang/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: KhachHang/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MaKhachHang,TenKhachHang,SDT,DiaChi,LienHe")] KhachHang khachHang)
        {
            if (ModelState.IsValid)
            {
                if(_serviceV.CheckPhoneNumberExist(khachHang.SDT) )
                {
                _context.Add(khachHang);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
                }
                else
                {
                    ViewData["ExistPhoneNumber"] = "Số điện thoại đã được đăng ký";
                    return View();
                }
            }
            return View(khachHang);
        }

        // GET: KhachHang/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khachHang = await _context.KhachHangs.FindAsync(id);
            if (khachHang == null)
            {
                return NotFound();
            }
            return View(khachHang);
        }

        // POST: KhachHang/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MaKhachHang,TenKhachHang,SDT,DiaChi,LienHe")] KhachHang khachHang)
        {
            if (id != khachHang.MaKhachHang)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(khachHang);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KhachHangExists(khachHang.MaKhachHang))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(khachHang);
        }

        // GET: KhachHang/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var khachHang = await _context.KhachHangs
                .FirstOrDefaultAsync(m => m.MaKhachHang == id);
            if (khachHang == null)
            {
                return NotFound();
            }

            return View(khachHang);
        }

        // POST: KhachHang/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var khachHang = await _context.KhachHangs.FindAsync(id);
            _context.KhachHangs.Remove(khachHang);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool KhachHangExists(int id)
        {
            return _context.KhachHangs.Any(e => e.MaKhachHang == id);
        }
    }
}
