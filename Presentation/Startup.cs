using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using ApplicationCore.Interfaces.IRepository;
using ApplicationCore.Interfaces.IServices;
using ApplicationCore.Services;
using Infrastructure;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Presentation.ServiceViews;

namespace Presentation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<MeMoMay>(options =>
           options.UseSqlite(Configuration.GetConnectionString("MeMoMay")));
            //UnitOfWork
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            //1.SanPham
            services.AddScoped<ISanPhamRepository, SanPhamRepository>();
            services.AddScoped<ISanPhamService, SanPhamService>();
            services.AddScoped<SanPhamServiceView>();
            //2.NhanVien
            services.AddScoped<INhanVienRepository, NhanVienRepository>();
            services.AddScoped<INhanVienService, NhanVienService>();
            //3.DonHang
            services.AddScoped<IDonHangRepository, DonHangRepository>();
            services.AddScoped<IDonHangService, DonHangService>();
            services.AddScoped<DonHangServiceView>();
            //4.KhachHang
            services.AddScoped<IKhachHangRepository, KhachHangRepository>();
            services.AddScoped<IKhachHangService, KhachHangService>();
            services.AddScoped<KhachHangServiceView>();
            //5.BoSuuTap
            services.AddScoped<IBoSuuTapRepositoty, BoSuuTapRepository>();
            services.AddScoped<IBoSuuTapService, BoSuuTapService>();
            //6.ChiTietNhapHang
            services.AddScoped<IChiTietNhapHangRepository, ChiTietNhapHangRepository>();
            services.AddScoped<IChiTietNhapHangService, ChiTietNhapHangService>();
            //7.ChiTietDonHang
            services.AddScoped<IChiTietDonHangRepository, ChiTietDonHangRepository>();
            services.AddScoped<IChiTietDonHangService, ChiTietDonHangService>();
            //8.ThongTinNhapHang
            services.AddScoped<IThongTinNhapHangRepository, ThongTinNhapHangRepository>();
            services.AddScoped<IThongTinNhapHangService, ThongTinNhapHangService>();
            services.AddScoped < ThongTinNhapHangServiceView>();
            //9.Home
            services.AddScoped<HomeServiceView>();
            //10.ThongKe
            services.AddScoped<ThongKeServiceView>();
            //Add session
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Account}/{action=Index}/{id?}");
            });
        }
    }
}
