using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IServices;
using ApplicationCore.ViewModels;

namespace Presentation.ServiceViews
{
    public class KhachHangServiceView
    {
        private readonly IKhachHangService _serviceKH;
        private readonly IDonHangService _serviceDH;
        private int pageSize = 10;
        public KhachHangServiceView (IKhachHangService serviceKH,IDonHangService serviceDH)
        {
            _serviceDH = serviceDH;
            _serviceKH = serviceKH;
        }
        public KhachHangIndexVM GetHangIndexVM(string CurrentFillter,int pageIndex = 1)
        {
            List<KhachHangVM> vm = new List<KhachHangVM>();
            var khachHangs = _serviceKH.GetAll();
            foreach(var item in khachHangs)
            {
                var donHangs = from dh in _serviceDH.GetAll()
                               where dh.MaKhachHang == item.MaKhachHang && dh.TinhTrang != "Đã hủy"
                               select dh;
                double tongTien = TinhTongTien(donHangs);
                int soLuongDonHang = donHangs.ToList().Count();
                KhachHangVM kh = new KhachHangVM();
                kh.khachHang = item;
                kh.soLuongDonHang = soLuongDonHang;
                kh.tongTien = tongTien;
                vm.Add(kh);
            }
            if (!String.IsNullOrEmpty(CurrentFillter))
            {
                vm = vm.Where(t => t.khachHang.TenKhachHang.ToLower().Contains(CurrentFillter.ToLower()) || t.khachHang.SDT.Contains(CurrentFillter)).ToList();
            }
            return new KhachHangIndexVM
            {
                KhachHangs = PaginatedList<KhachHangVM>.Create(vm, pageIndex, pageSize)
            };
        }
        public double TinhTongTien(IEnumerable<DonHang> donHangs)
        {
            double result = 0;
            foreach(var item in donHangs)
            {
                result += item.TongTien;
            }
            return result;
        }
        public bool CheckPhoneNumberExist(string phoneNumber)
        {
            var listPhoneNumber = from kh in _serviceKH.GetAll()
                                  select kh.SDT;
            foreach(var item in listPhoneNumber)
            {
                if(phoneNumber == item)
                    return false;
            }
            return true;
        }
    }
}