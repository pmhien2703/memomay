using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ApplicationCore;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IServices;
using ApplicationCore.ViewModels;
using Microsoft.AspNetCore.Http;
using Presentation.Controllers;

namespace Presentation.ServiceViews
{
    public class ThongTinNhapHangServiceView
    {
        private readonly int pageSize = 12;
        private readonly IThongTinNhapHangService _serviceTTNH;
        private readonly ISanPhamService _serviceSP;
        private readonly IChiTietNhapHangService _serviceCTNH;
        public ThongTinNhapHangServiceView(IThongTinNhapHangService serviceTTNH, ISanPhamService serviceSP,IChiTietNhapHangService serviceCTNH)
        {
            _serviceSP = serviceSP;
            _serviceTTNH = serviceTTNH;
            _serviceCTNH = serviceCTNH;
        }
        public ThongTinNhapHangIndexVM GetThongTinNhapHangIndex(int pageIndex = 1)
        {
            var nhaphang = _serviceTTNH.GetAll().OrderByDescending(m => m.NgayNhapHang.Date).ThenByDescending(s => s.NgayNhapHang.TimeOfDay);
            int count = nhaphang.Count();
            return new ThongTinNhapHangIndexVM
            {
                ThongTinNhapHangs = PaginatedList<ThongTinNhapHang>.Create(nhaphang, pageIndex, pageSize)
            };
        }
        public ThongTinNhapHangCreateVM CreateVM()
        {
            ThongTinNhapHangCreateVM vm = new ThongTinNhapHangCreateVM();
            vm.SanPhams = _serviceSP.GetAllName();
            return vm;
        }
        public ThongTinNhapHangCreateVM CreateVM(ThongTinNhapHangCreateVM vm)
        {
            ThongTinNhapHangCreateVM newvm = new ThongTinNhapHangCreateVM();
            newvm.SanPhams = _serviceSP.GetAllName();
            if(ThongTinNhapHangController.listtam == null)
            {
                ThongTinNhapHangController.listtam = new List<ChiTietNhapHangVM>();
            }

            //id sản phẩm lấy trên ui không phải là id đúng vì đều là size 1

            //getbyid để lấy tên và màu
            SanPham sp = _serviceSP.GetById(vm.chiTietNhapHangVM.MaSanPham);

            //lấy sản phẩm từ tên,size,màu => nhằm lấy đúng size
            if(vm.chiTietNhapHangVM.Size != 0)
            {
                sp = _serviceSP.GetByNameAndSizeAndColor(sp.TenSanPham, vm.chiTietNhapHangVM.Size.ToString(), sp.Mau);
            }

            //cập nhật thông tin cho CTNHVM
            vm.chiTietNhapHangVM.TenSanPham = sp.TenSanPham;
            vm.chiTietNhapHangVM.MaSanPham = sp.MaSanPham;
            vm.chiTietNhapHangVM.Mau = sp.Mau;
            // vm.chiTietNhapHangVM.TenSanPham = _serviceSP.GetById(vm.chiTietNhapHangVM.MaSanPham).TenSanPham;

            ThongTinNhapHangController.listtam.Add(vm.chiTietNhapHangVM);
            newvm.ListChiTietNhapHangs = ThongTinNhapHangController.listtam;
            return newvm;
        }
        public void UpdateSLT(int id, int soLuong)
        {
            SanPham sp = _serviceSP.GetById(id);
            sp.SoLuongTon = sp.SoLuongTon + soLuong;
            _serviceSP.Update(sp);
        }
        public void CreateTTNH(ThongTinNhapHangCreateVM vm,int MaNhanVien)
        {
            //Tao ThongTinNhapHang
            ThongTinNhapHang thongTinNhap = new ThongTinNhapHang();
            thongTinNhap.MaNhanVien = MaNhanVien;
            thongTinNhap.NgayNhapHang = DateTime.Now;
            _serviceTTNH.Add(thongTinNhap);
           foreach(var item in ThongTinNhapHangController.listtam)
           {
               if(item.Size != 0)
               {
                   //Update SLL SP
                    var idsp = item.MaSanPham;
                    UpdateSLT(idsp, item.SoLuong);

                    //Tao Chi Tiet Nhap Hang
                    ChiTietNhapHang ctnh = new ChiTietNhapHang();
                    ctnh.MaNhapHang = thongTinNhap.MaNhapHang;
                    ctnh.MaSanPham = idsp;
                    ctnh.SoLuong = item.SoLuong;
                    _serviceCTNH.Update(ctnh);
                }
                else
                {
                    SanPham sp = _serviceSP.GetById(item.MaSanPham);
                    if(item.Ri == "1-4")
                    {
                        for (int i = 1; i <= 4;i++)
                        {
                            var idsp = _serviceSP.GetByNameAndSizeAndColor(sp.TenSanPham,i.ToString(),sp.Mau).MaSanPham;
                            UpdateSLT(idsp, item.SoLuong);
                            //Tao Chi Tiet Nhap Hang
                            ChiTietNhapHang ctnh = new ChiTietNhapHang();
                            ctnh.MaNhapHang = thongTinNhap.MaNhapHang;
                            ctnh.MaSanPham = idsp;
                            ctnh.SoLuong = item.SoLuong;
                            _serviceCTNH.Update(ctnh);
                        }
                    }
                    else if (item.Ri == "1-8")
                    {
                        for (int i = 1; i <= 8; i++)
                        {
                            var idsp = _serviceSP.GetByNameAndSizeAndColor(sp.TenSanPham, i.ToString(), sp.Mau).MaSanPham;
                            UpdateSLT(idsp, item.SoLuong);
                            //Tao Chi Tiet Nhap Hang
                            ChiTietNhapHang ctnh = new ChiTietNhapHang();
                            ctnh.MaNhapHang = thongTinNhap.MaNhapHang;
                            ctnh.MaSanPham = idsp;
                            ctnh.SoLuong = item.SoLuong;
                            _serviceCTNH.Update(ctnh);

                        }
                    }
                    else if (item.Ri == "5-8")
                    {
                        for (int i = 5; i <= 8; i++)
                        {
                            var idsp = _serviceSP.GetByNameAndSizeAndColor(sp.TenSanPham, i.ToString(), sp.Mau).MaSanPham;
                            UpdateSLT(idsp, item.SoLuong);
                            //Tao Chi Tiet Nhap Hang
                            ChiTietNhapHang ctnh = new ChiTietNhapHang();
                            ctnh.MaNhapHang = thongTinNhap.MaNhapHang;
                            ctnh.MaSanPham = idsp;
                            ctnh.SoLuong = item.SoLuong;
                            _serviceCTNH.Update(ctnh);
                        }
                    }
                    else if (item.Ri == "9-12")
                    {
                        for (int i = 9; i <= 12; i++)
                        {
                            var idsp = _serviceSP.GetByNameAndSizeAndColor(sp.TenSanPham, i.ToString(), sp.Mau).MaSanPham;
                            UpdateSLT(idsp, item.SoLuong);
                            //Tao Chi Tiet Nhap Hang
                            ChiTietNhapHang ctnh = new ChiTietNhapHang();
                            ctnh.MaNhapHang = thongTinNhap.MaNhapHang;
                            ctnh.MaSanPham = idsp;
                            ctnh.SoLuong = item.SoLuong;
                            _serviceCTNH.Update(ctnh);

                        }
                    }
                    else if (item.Ri == "1-12")
                    {
                        for (int i = 1; i <= 12; i++)
                        {
                            var idsp = _serviceSP.GetByNameAndSizeAndColor(sp.TenSanPham, i.ToString(), sp.Mau).MaSanPham;
                            UpdateSLT(idsp, item.SoLuong);
                            //Tao Chi Tiet Nhap Hang
                            ChiTietNhapHang ctnh = new ChiTietNhapHang();
                            ctnh.MaNhapHang = thongTinNhap.MaNhapHang;
                            ctnh.MaSanPham = idsp;
                            ctnh.SoLuong = item.SoLuong;
                            _serviceCTNH.Update(ctnh);
                        }
                    }
                }
           }
        }
        public IEnumerable<ChiTietNhapHangVM> Details(int id)
        {
            Expression<Func<ChiTietNhapHang, bool>> predicate = m => true;
            predicate = m => m.MaNhapHang == id;
            var nhapHang = _serviceCTNH.Find(predicate);
            IEnumerable<ChiTietNhapHangVM> result = from ctnh in nhapHang
                                                    join sp in _serviceSP.GetAll()
                                                    on ctnh.MaSanPham equals sp.MaSanPham
                                                    select new ChiTietNhapHangVM
                                                    {
                                                        TenSanPham = sp.TenSanPham,
                                                        Size = Int32.Parse(sp.Size),
                                                        SoLuong = ctnh.SoLuong
                                                    };
            return result;
        }
    }
}