using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IServices;
using ApplicationCore.ViewModels;

namespace Presentation.ServiceViews
{
    public class HomeServiceView
    {
        private readonly IKhachHangService _serviceKH;
        private readonly IDonHangService _serviceDH;
        public HomeServiceView (IKhachHangService serviceKH,IDonHangService serviceDH)
        {
            _serviceDH = serviceDH;
            _serviceKH = serviceKH;
        }
        public HomeIndexVM GetIndexModel()
        {
            var now = DateTime.Now;
            var startOfLastMonth = new DateTime(now.Year, now.AddMonths(1).Month, 1); 
            var startOfMonth = new DateTime(now.Year, now.Month, 1);
            var lastDay = new DateTime(now.Year, now.Month +1 , 1);

            var donHangThangNay = from dh in _serviceDH.GetAll()
                                 where dh.Ngay >= startOfMonth && dh.Ngay <= lastDay && dh.TinhTrang != "Đã hủy"
                                 select dh;
            var donHangThangTruoc =from dh in _serviceDH.GetAll()
                                   where dh.Ngay >= startOfLastMonth && dh.Ngay <= startOfMonth && dh.TinhTrang != "Đã hủy"
                                   select dh;
            double doanhThuThangNay = DoanhThuThang(donHangThangNay);
            double doanhThuThangTruoc = DoanhThuThang(donHangThangTruoc);
            double phanTramDoanhThu = TinhPhanTram(doanhThuThangTruoc, doanhThuThangNay);
            double SLdonHangThangNay = donHangThangNay.ToList().Count();
            double SLdonHangThangTruoc = donHangThangTruoc.ToList().Count();
            double phanTramDonHang = TinhPhanTram(SLdonHangThangTruoc, SLdonHangThangNay);
            HomeIndexVM vm = new HomeIndexVM();
            vm.SoLuongDonHang = (int)SLdonHangThangNay;
            vm.DoanhThuThangHienTai = doanhThuThangNay;
            vm.SoLuongKhachHang = _serviceKH.GetAll().ToList().Count();
            vm.PhanTramDoanhThu = phanTramDoanhThu;
            vm.PhanTramDonHang = phanTramDonHang;
            return vm;
        }
        public double DoanhThuThang(IEnumerable<DonHang> donHangs)
        {
            double result = 0;
            foreach(var item in donHangs)
            {
                result += item.TongTien;
            }
            return result;
        }
        public double TinhPhanTram(double lastmonth,double thismonth)
        {
            double result =  (thismonth/lastmonth)*100 ;
            if(thismonth < lastmonth)
                result = 100 - result;
            return result;
        }
    }
}