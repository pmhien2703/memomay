using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ApplicationCore;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IServices;
using ApplicationCore.ViewModels;
using Presentation.Controllers;

namespace Presentation.ServiceViews
{
    public class DonHangServiceView
    {
        private readonly int pageSize =12;
        private readonly IDonHangService _serviceDH;
        private readonly ISanPhamService _serviceSP;
        private readonly IKhachHangService _serviceKH;
        private readonly IChiTietDonHangService _serviceCTDH;
        public DonHangServiceView ( IDonHangService serviceDH,IKhachHangService serviceKH,ISanPhamService serviceSP,IChiTietDonHangService serviceCTDH)
        {
            _serviceDH = serviceDH;
            _serviceSP = serviceSP;
            _serviceKH = serviceKH;
            _serviceCTDH = serviceCTDH;
        }
        public double TinhTongTien(List<ThongTinChiTietDonHangVM> list)
        {
            double result = 0;
            foreach(var item in list)
            {
                result += item.ThanhTien;
            }
            return result;
        }
        public void CapNhatTongTien(int id)
        {
            Expression<Func<ChiTietDonHang, bool>> predicate = m => true;
            predicate = m => m.MaDonHang == id;
            IEnumerable<ChiTietDonHang> listCTDH = _serviceCTDH.Find(predicate);
            double tongtien = 0;
            foreach(var item in listCTDH)
            {
                tongtien = tongtien + item.ThanhTien;
            }
            DonHang donHang = _serviceDH.GetById(id);
            donHang.TongTien = tongtien;
            _serviceDH.Update(donHang);
        }
        public void CapNhatThanhTien(int idctdh,int idsanpham)
        {
            ChiTietDonHang chiTietDonHang = _serviceCTDH.GetById(idctdh);
            SanPham sp = _serviceSP.GetById(idsanpham);
            chiTietDonHang.ThanhTien = (chiTietDonHang.SoLuong * sp.GiaBan) - (chiTietDonHang.SoLuong * sp.GiaBan * chiTietDonHang.ChietKhau / 100) ;
            _serviceCTDH.Update(chiTietDonHang);
        }
        public DonHangCreateVM CreateDHVM(DonHangCreateVM donHang)
        {
            //New model
            DonHangCreateVM vm = new DonHangCreateVM();
            vm.ThongTinSP = _serviceSP.GetAllName();
            vm.ThongTinKH = _serviceKH.GetAll();

            //chọn masanpham lại theo size 
            SanPham sp = _serviceSP.GetById(donHang.sanPham.MaSanPham);
            int idsp = _serviceSP.GetByNameAndSizeAndColor(sp.TenSanPham,donHang.sanPham.Size.ToString(),sp.Mau).MaSanPham;
            double giaban = _serviceSP.GetById(idsp).GiaBan;

            //Tạo 1 ctdh viewmodel
            ThongTinChiTietDonHangVM ctdh = new ThongTinChiTietDonHangVM
            {
                MaSanPham = idsp,
                TenSanPham = donHang.sanPham.TenSanPham,
                SoLuong = donHang.sanPham.SoLuongTon,
                Size = donHang.sanPham.Size,
                ThanhTien = (donHang.sanPham.SoLuongTon * (giaban)) - (donHang.sanPham.SoLuongTon * (giaban) * donHang.ChietKhau / 100),
                DonGia = giaban,
                ChietKhau = donHang.ChietKhau,
                Mau = _serviceSP.GetById(idsp).Mau,
            };
            if (DonHangController.listtam == null)
            {
                DonHangController.listtam = new List<ThongTinChiTietDonHangVM>();
            }
            DonHangController.listtam.Add(ctdh);
            vm.ThongTinCTDH = DonHangController.listtam;
            vm.khachHang = donHang.khachHang;
            vm.ThongTinDH = donHang.ThongTinDH;
            double tongtien = 0;
            for (int i = 0; i < DonHangController.listtam.Count; i++)
            {
                tongtien += DonHangController.listtam[i].ThanhTien;
            }
            if (donHang.ThongTinDH == null)
            {
                donHang.ThongTinDH = new DonHang();
            }
            vm.ThongTinDH = donHang.ThongTinDH;
            vm.ThongTinDH.TongTien = tongtien;
            vm.TongTien = tongtien;
            return vm;
        }

        public DonHangCreateVM RemoveOneProduct(DonHangCreateVM donHangCreateVM)
        {
            int idsp = 0;
            foreach(var item in donHangCreateVM.ThongTinCTDH)
            {
                idsp = item.MaSanPham;
            }

            for (int i = 0; i < DonHangController.listtam.Count; i++)
            {
                if(DonHangController.listtam[i].MaSanPham == idsp)
                {
                    DonHangController.listtam.RemoveAt(i);
                }
            }
            DonHangCreateVM vm = new DonHangCreateVM();
            vm.ThongTinSP = _serviceSP.GetAllName();
            vm.ThongTinKH = _serviceKH.GetAll();
            vm.ThongTinCTDH = DonHangController.listtam;
            vm.khachHang = donHangCreateVM.khachHang;
            double tongtien = 0;
            for (int i = 0; i < DonHangController.listtam.Count; i++)
            {
                tongtien += DonHangController.listtam[i].ThanhTien;
            }
            if (donHangCreateVM.ThongTinDH == null)
            {
                donHangCreateVM.ThongTinDH = new DonHang();
            }
            vm.ThongTinDH = donHangCreateVM.ThongTinDH;
            vm.ThongTinDH.TongTien = tongtien;
            vm.TongTien = tongtien;
            return vm;
        }
        public void CapNhapHangTonKho(int id,int number)
        {
            SanPham sp = _serviceSP.GetById(id);
            sp.SoLuongTon += number;
            _serviceSP.Update(sp);
        }
        public void CreateDonHang(DonHangCreateVM donHangCreateVM)
        {
            //Tạo đơn hàng mới
            DonHang donHang = new DonHang();
            donHang = donHangCreateVM.ThongTinDH;
            donHang.MaKhachHang = donHangCreateVM.khachHang.MaKhachHang;
            if(donHangCreateVM.ThongTinDH.Ngay == null)
            {
                donHang.Ngay = DateTime.Now;
            }
            else
            {
                donHang.Ngay = donHangCreateVM.ThongTinDH.Ngay;
            }
            donHang.TinhTrang = "Đã tạo đơn";
            donHang.TongTien = TinhTongTien(DonHangController.listtam);
            
            _serviceDH.Add(donHang);
            
            foreach (var item in DonHangController.listtam)
            {
                //Tạo chi tiết đơn hàng mới
                ChiTietDonHang ctdh = new ChiTietDonHang();
                ctdh.MaSanPham = item.MaSanPham;
                ctdh.SoLuong = item.SoLuong;
                ctdh.ThanhTien = item.ThanhTien;
                ctdh.MaDonHang = donHang.MaDonHang;
                ctdh.ChietKhau = item.ChietKhau;
                _serviceCTDH.Add(ctdh);

                //Trừ số lượng hàng tồn
                CapNhapHangTonKho(ctdh.MaSanPham, -ctdh.SoLuong);
            }
            //Xóa object sau khi đã lưu
            DonHangController.listtam = null;
        }
        public DonHangIndexVM GetDonHangIndex(string sort, int pageIndex = 1)
        {
            var donhang = _serviceDH.GetAll();

            if (!String.IsNullOrEmpty(sort))
            {
                if(sort != "Tất cả")
                    donhang = from dh in donhang
                              where dh.TinhTrang.Equals(sort)
                              select dh;
                donhang.OrderByDescending(m => m.Ngay.Date).ThenByDescending(s => s.Ngay.TimeOfDay);
            }
            int count = donhang.Count();
            return new DonHangIndexVM
            {
                DonHangs = PaginatedList<DonHang>.Create(donhang,pageIndex,pageSize)
            };
        }
        public bool CheckSizeExit(string TenSanPham,string Size)
        {
            foreach(var item in _serviceSP.GetByName(TenSanPham))
            {
                if(Size == item.Size)
                    return true;
            }
            return false;
        }
        public void ChangeStatus(int id,string status,string sort,int pageindex)
        {
            //ChangeStatus
            DonHang donHang = _serviceDH.GetById(id);
            donHang.TinhTrang = status;
            _serviceDH.Update(donHang);
            //UpdateSLT
            if(status == "Đã hủy")
            {
                Expression<Func<ChiTietDonHang, bool>> predicate = m => true;
                predicate = m => m.MaDonHang == id;
                List<ChiTietDonHang> ctdh = _serviceCTDH.Find(predicate).ToList();
                for (int i = 0; i < ctdh.Count();i++)
                {
                    CapNhapHangTonKho(ctdh[i].MaSanPham, ctdh[i].SoLuong);
                }
            }
        }
        public bool ChangeQuantity(int id,int quantity,int idsanpham)
        {
            ChiTietDonHang chiTietDonHang = _serviceCTDH.GetById(id);
            int number = quantity - chiTietDonHang.SoLuong;
            if(_serviceSP.GetById(idsanpham).SoLuongTon < number || quantity == 0) 
                return false;
            //Cập nhật hàng tồn kho
            CapNhapHangTonKho(idsanpham, -number);
            //Cập nhật số lượng của đơn hàng, tổng tiền, thành tiền
            chiTietDonHang.SoLuong = quantity;
            _serviceCTDH.Update(chiTietDonHang);
            CapNhatThanhTien(id, idsanpham);
            CapNhatTongTien(chiTietDonHang.MaDonHang);
            return true;
        }
        public bool ChangeSize(int stt,string size,int idsanpham)
        {
            ChiTietDonHang chiTietDonHang = _serviceCTDH.GetById(stt);
            //Cập nhật hàng tồn kho - size mới
            SanPham sp = _serviceSP.GetById(idsanpham);
            int spNew = _serviceSP.GetByNameAndSizeAndColor(sp.TenSanPham, size, sp.Mau).MaSanPham;
            if(_serviceSP.GetById(spNew).SoLuongTon < chiTietDonHang.SoLuong || !CheckSizeExit(_serviceSP.GetById(spNew).TenSanPham,size))
                return false;
            CapNhapHangTonKho(spNew, -chiTietDonHang.SoLuong);
            //Cập nhật hàng tồn kho - size cũ
            CapNhapHangTonKho(idsanpham, chiTietDonHang.SoLuong);
            
            //Cập nhật chi tiết đơn hàng mới
            chiTietDonHang.MaSanPham = spNew;
            _serviceCTDH.Update(chiTietDonHang);
            return true;
        }
        public bool RemoveOrderLine(int id)
        {
            ChiTietDonHang chiTietDonHang = _serviceCTDH.GetById(id);
            _serviceCTDH.Remove(chiTietDonHang);
            //Cập nhật hàng tồn kho
            CapNhapHangTonKho(chiTietDonHang.MaSanPham, chiTietDonHang.SoLuong);
            //Cập nhật tổng tiền
            CapNhatTongTien(chiTietDonHang.MaDonHang);
            return true;
        }
        public DonHangEditVM Detail(int id)
        {
            //Lấy chi tiết đơn hàng
            Expression<Func<ChiTietDonHang, bool>> predicate = m => true;
            predicate = m => m.MaDonHang == id;
            var listCTDT = _serviceCTDH.Find(predicate);
            IEnumerable<ThongTinChiTietDonHangVM> result = from ctdh in listCTDT
                                                           join sp in _serviceSP.GetAll()
                                                           on ctdh.MaSanPham equals sp.MaSanPham
                                                           select new ThongTinChiTietDonHangVM
                                                           {
                                                               TenSanPham = sp.TenSanPham,
                                                               SoLuong = ctdh.SoLuong,
                                                               ThanhTien = ctdh.ThanhTien,
                                                               Size = sp.Size,
                                                               DonGia = sp.GiaBan,
                                                               SoThuTu = ctdh.SoThuTu,
                                                               MaSanPham = ctdh.MaSanPham,
                                                               ChietKhau = ctdh.ChietKhau,
                                                            };
            DonHangEditVM vm = new DonHangEditVM();
            vm.listCTDT = result;
            //Lấy thông tin đơn hàng
            var donHang = _serviceDH.GetById(id);
            vm.donHang = donHang;
            //Lấy tên khách hàng
            string tenKH = _serviceKH.GetById(donHang.MaKhachHang).TenKhachHang;
            vm.TenKhachHang = tenKH;
            return vm;
        }
        public bool ChangeDiscount(int id, double discount)
        {
            if (discount < 0 || discount >100)
                return false;
            //Cập nhật chi tiết đơn hàng
            ChiTietDonHang chiTietDonHang = _serviceCTDH.GetById(id);
            chiTietDonHang.ChietKhau = discount;
            _serviceCTDH.Update(chiTietDonHang);
            //Cập nhật tổng tiền, thành tiền
            CapNhatThanhTien(id, chiTietDonHang.MaSanPham);
            CapNhatTongTien(chiTietDonHang.MaDonHang);
            return true;
        }
    }
}