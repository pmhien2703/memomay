using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IServices;
using ApplicationCore.ViewModels;

namespace Presentation.ServiceViews
{
    public class ThongKeServiceView
    {
        private readonly IDonHangService _serviceDH;
        private readonly DonHangServiceView _serviceDHV;
        private readonly ISanPhamService _serviceSP;
        private readonly IChiTietDonHangService _serviceCTDH;
        public ThongKeServiceView(IDonHangService serviceDH,ISanPhamService serviceSP, IChiTietDonHangService serviceCTDT,DonHangServiceView serviceView)
        {
            _serviceSP = serviceSP;
            _serviceDH = serviceDH;
            _serviceCTDH = serviceCTDT;
            _serviceDHV = serviceView;
        }
        public double TinhTienNhapHang(int id)
        {
            Expression<Func<ChiTietDonHang, bool>> predicate = m => true;
            predicate = m => m.MaDonHang == id;
            IEnumerable<ChiTietDonHang> chiTietDonHangs = _serviceCTDH.Find(predicate);
            double TienNhapHang = 0;
            foreach(var item in chiTietDonHangs)
            {
                if(_serviceDH.GetById(id).TinhTrang != "Đã hủy")
                TienNhapHang = TienNhapHang + (_serviceSP.GetById(item.MaSanPham).GiaGoc * item.SoLuong );
            }
            return TienNhapHang;
        }
        public double TinhDoanhThu(List<DonHang> listDonHang)
        {
            double result= 0;
            foreach(DonHang item in listDonHang)
            {
                if(item.TinhTrang != "Đã hủy")
                result = result + item.TongTien;
            }
            return result;
        }
        public double TinhTongTienShip(List<DonHang> listDonHang)
        {
            double result = 0;
            foreach (DonHang item in listDonHang)
            {
                result = result + item.ChiPhiVanChuyen;
            }
            return result;
        }
        
        public double TinhLoiNhuan(double DoanhThu,double TienShip,double TienNhapHang)
        {
            return DoanhThu - TienShip - TienNhapHang;
        }
        public List<DonHang> GetDonHangFromTo(DateTime From, DateTime To)
        {
            To = To.AddDays(1);
            Expression<Func<DonHang, bool>> predicate = m => true;
            predicate = m => m.Ngay >= From && m.Ngay <= To && m.TinhTrang != "Đã hủy";
            return _serviceDH.Find(predicate).ToList();
        }
        public List<DonHang> GetDonHangFromToE(DateTime From, DateTime To)
        {
            To = To.AddDays(1);
            Expression<Func<DonHang, bool>> predicate = m => true;
            predicate = m => m.Ngay >= From && m.Ngay <= To;
            return _serviceDH.Find(predicate).ToList();
        }
        public ThongKeDonHangVM GetThongKeDonHangVM (DateTime From,DateTime To)
        {
            ThongKeDonHangVM vm = new ThongKeDonHangVM();
            vm.listDonHang = new List<ThongKeDonHangModel>();
            double TongTienNhapHang = 0;
            //Danh sách đơn hàng trong khoãng tgian thống kê
            List<DonHang> donHangs = GetDonHangFromToE(From,To);
            //Tính tiền nhập hàng
            foreach(var item in donHangs)
            {
                ThongKeDonHangModel model = new ThongKeDonHangModel();
                model.DonHang = item;
                model.GiaGoc = TinhTienNhapHang(item.MaDonHang);
                TongTienNhapHang = model.GiaGoc + TongTienNhapHang;
                vm.listDonHang.Add(model);
            }
            //Thêm các thuộc tính của VM
            vm.TongSoDonHang = donHangs.Count();
            vm.DoanhThu = TinhDoanhThu(donHangs);
            vm.TongTienShip = TinhTongTienShip(donHangs);
            vm.TienNhapHang = TongTienNhapHang;
            vm.LoiNhuan = TinhLoiNhuan(vm.DoanhThu, vm.TongTienShip, vm.TienNhapHang);
            return vm;
        }
        public bool CheckedName(List<SanPham> list,SanPham sp)
        {
            foreach ( SanPham item in list)
            {
                if(sp.TenSanPham == item.TenSanPham)
                    return false;
            }
            return true;
        }
        public List<SanPham> LapDanhSachTenSanPham(List<ChiTietDonHang> list)
        {
            List<SanPham> listSP = new List<SanPham>();
                foreach(ChiTietDonHang element in list)
                {
                    SanPham sp = new SanPham();
                    sp = _serviceSP.GetById(element.MaSanPham);
                    if(CheckedName(listSP,sp))
                    {
                        listSP.Add(sp);
                    }
                }
            return listSP;
        }
        public List<SanPham> LapDanhSachMauSanPham(List<ChiTietDonHang> list)
        {
            List<SanPham> listSP = new List<SanPham>();
            foreach (ChiTietDonHang element in list)
            {
                SanPham sp = new SanPham();
                sp = _serviceSP.GetById(element.MaSanPham);
                if (CheckedColor(listSP, sp))
                {
                    listSP.Add(sp);
                }
            }
            return listSP;
        }

        private bool CheckedColor(List<SanPham> listSP, SanPham sp)
        {
            foreach(SanPham item in listSP)
            {
                if(item.Mau == sp.Mau)
                return false;
            }
            return true;
        }

        public int CountQuantityPerSizeOfProduct(List<ChiTietDonHang> list,string Size,SanPham sp)
        {
            int result = 0;
            foreach(ChiTietDonHang item in list)
            {   
                if(item.MaSanPham == _serviceSP.GetByNameAndSizeAndColor(sp.TenSanPham,Size,sp.Mau).MaSanPham)
                {
                    result += item.SoLuong;
                }
            }
            return result;
        }
        public List<ChiTietDonHang> LapDanhSachCTDH(List<DonHang> donHangs)
        {
            List<ChiTietDonHang> listCTDH = new List<ChiTietDonHang>();
            foreach (var item in donHangs)
            {
                Expression<Func<ChiTietDonHang, bool>> predicate = m => true;
                predicate = m => m.MaDonHang == item.MaDonHang;
                listCTDH.AddRange(_serviceCTDH.Find(predicate));
            }
            return listCTDH;
        }
        public List<ThongKeSanPhamVM> GetThongKeSanPhamVMs(DateTime From,DateTime To)
        {
            List<ThongKeSanPhamVM> vm = new List<ThongKeSanPhamVM>();
            //Danh sách đơn hàng trong khoãng tgian thống kê
            List<DonHang> donHangs = GetDonHangFromTo(From, To);
            //Danh sách chi tiết đơn hàng của đơn hàng
            List<ChiTietDonHang> listCTDH = LapDanhSachCTDH(donHangs);
            //Lập danh sách sản phẩm được bán trong thời gian đó
            List<SanPham> listSP = LapDanhSachTenSanPham(listCTDH);

            foreach(SanPham item in listSP)
            {
                //lập list size cho mỗi sản phẩm
                List<int> listQuantityPerSize = new List<int>();
                
                for (int i = 1; i <= 12;i++)
                {
                    listQuantityPerSize.Add(CountQuantityPerSizeOfProduct(listCTDH, i.ToString(), item));
                }
                //Tạo model
                ThongKeSanPhamVM model = new ThongKeSanPhamVM();
                model.TenSanPham = item.TenSanPham;
                model.SoLuongMoiSize = listQuantityPerSize;
                model.SoLuongSanPham = listQuantityPerSize.Sum();
                model.img = item.HinhAnh;
                //
                vm.Add(model);
            }
            return vm.OrderByDescending(m=>m.SoLuongSanPham).ToList();
        }
        public List<ThongKeMauVM> GetThongKeMauVM(DateTime From, DateTime To)
        {
            List<ThongKeMauVM> vm = new List<ThongKeMauVM>();
            //Danh sách đơn hàng trong khoảng thời gian thống kê
            List<DonHang> donHangs = GetDonHangFromTo(From, To);
            //Danh sách chi tiết đơn hàng của đơn hàng
            List<ChiTietDonHang> listCTDH = LapDanhSachCTDH(donHangs);
            //Danh sách màu
            List<SanPham> listMau = LapDanhSachMauSanPham(listCTDH);
            foreach(SanPham sanPham in listMau)
            {
                ThongKeMauVM model = new ThongKeMauVM();
                model.TenMau = sanPham.Mau;
                model.SoLuong = 0;
                foreach (ChiTietDonHang item in listCTDH)
                {
                    if(_serviceSP.GetById(item.MaSanPham).Mau.ToUpper() == sanPham.Mau.ToUpper())
                    {
                        model.SoLuong += item.SoLuong;
                    }
                }
                vm.Add(model);
            }
            return vm.OrderByDescending(m=>m.SoLuong).ToList();
        }
    }
}