using System;
using System.Linq;
using ApplicationCore;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces.IServices;
using ApplicationCore.ViewModels;
using Microsoft.AspNetCore.Http;

namespace Presentation.ServiceViews
{
    public class SanPhamServiceView
    {
        private readonly ISanPhamService _service;
        private readonly IBoSuuTapService _serviceBST;
        private int pageSize = 8;
        public SanPhamServiceView(ISanPhamService service,IBoSuuTapService serviceBST)
        {
            _service = service;
            _serviceBST = serviceBST;
        }
        public SanPhamIndexVM GetSanPhamIndexVM(string CurrentFilter,string sort ,int pageIndex = 1)
        {

            var sanPham = _service.GetAllByName();
            if(!String.IsNullOrEmpty(CurrentFilter) )
            {
                sanPham = sanPham.Where(t => t.TenSanPham.ToLower().Contains(CurrentFilter.ToLower())).ToList();
            }
            if(!String.IsNullOrEmpty(sort))
            {
                sanPham = sanPham.Where(m => m.TenBoSuuTap == sort).ToList();
            }
            return new SanPhamIndexVM
            {
                listBST = _serviceBST.GetAll(),
                SanPhams = PaginatedList<SanPhamVM>.Create(sanPham, pageIndex, pageSize)
            };
        }
        public SanPhamEditVM GetSanPhamEditVM(string Id)
        {
            SanPhamEditVM vm = new SanPhamEditVM();
            //Get san pham va gan ten san pham cho bien
            vm.sanPham = _service.GetById(Int32.Parse(Id));
            string TenSanPham = vm.sanPham.TenSanPham;
            string Color = vm.sanPham.Mau;

            vm.listBST = _serviceBST.GetAll();
            vm.Gia1 = _service.GetByNameAndSizeAndColor(TenSanPham, "1",Color).GiaGoc;
            vm.Gia2 = _service.GetByNameAndSizeAndColor(TenSanPham, "5",Color).GiaGoc;
            vm.Gia3 = _service.GetByNameAndSizeAndColor(TenSanPham, "9", Color).GiaGoc;
            vm.hangTonSize1 = _service.GetByNameAndSizeAndColor(TenSanPham,"1", Color).SoLuongTon;
            vm.hangTonSize2 = _service.GetByNameAndSizeAndColor(TenSanPham,"2", Color).SoLuongTon;
            vm.hangTonSize3 = _service.GetByNameAndSizeAndColor(TenSanPham,"3", Color).SoLuongTon;
            vm.hangTonSize4 = _service.GetByNameAndSizeAndColor(TenSanPham,"4", Color).SoLuongTon;
            vm.hangTonSize5 = _service.GetByNameAndSizeAndColor(TenSanPham,"5", Color).SoLuongTon;
            vm.hangTonSize6 = _service.GetByNameAndSizeAndColor(TenSanPham,"6", Color).SoLuongTon;
            vm.hangTonSize7 = _service.GetByNameAndSizeAndColor(TenSanPham,"7", Color).SoLuongTon;
            vm.hangTonSize8 = _service.GetByNameAndSizeAndColor(TenSanPham,"8", Color).SoLuongTon;
            vm.hangTonSize9 = _service.GetByNameAndSizeAndColor(TenSanPham,"9", Color).SoLuongTon;
            vm.hangTonSize10 = _service.GetByNameAndSizeAndColor(TenSanPham,"10", Color).SoLuongTon;
            vm.hangTonSize11 = _service.GetByNameAndSizeAndColor(TenSanPham,"11", Color).SoLuongTon;
            vm.hangTonSize12 = _service.GetByNameAndSizeAndColor(TenSanPham,"12", Color).SoLuongTon;
            return vm;
        }
    }
}